$(document).ready(function () {

	if($('#map-home').length){
		mapboxgl.accessToken = 'pk.eyJ1IjoiZW5kcml1cGl4dGVjaG5vc2VydmljZSIsImEiOiJjanJtY2l3cngwMWFzNDNvMHdpYmg2OGRtIn0.yxwskhVCY8dV7FG3_BhlbQ';
		var map = new mapboxgl.Map({
			container: 'map-home', // container id
			style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
			center: [8.8961465, 44.4085366], // starting position [lng, lat]
			zoom: 3, // starting zoom
			sprite: "mapbox://sprites/mapbox/streets-v8",
			glyphs: "mapbox://fonts/mapbox/{fontstack}/{range}.pbf",
		});
		//map['scrollZoom'].disable();
		var popupgenova = new mapboxgl.Popup()
			.setHTML("<strong>Genova</strong><p>Via Pietro Chiesa 9 - 3° piano - 16149 Genoa - ITALY</p>");
		var popupcagliari = new mapboxgl.Popup()
			.setHTML("<strong>Cagliari</strong><p>Via Ischia 4 - 09012 Capoterra - Cagliari - ITALY</p>");
		var popupboston = new mapboxgl.Popup()
			.setHTML("<strong>Boston</strong><p>125 Summer Street, Boston MA 02110 - USA</p>");
		var popupindia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE INDIA c/o INTERNATIONAL INSPECTION SERVICES (P) LTD </strong><p>Plot No. 29, # 5-6-137/6/4/2, Behind Metro, <br>Kukatpally, Hyderabad - 500 072 INDIA</p>");
		var popupcina = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE CHINA c/o THARSOS</strong><p>2701#, The South Tower, Huaninginternational plaza, No. 300 Xuanhua Road, <br>Changing Diostrict, Shanghai, CHINA</p>");
		var popupuk = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE U.K. c/o NEIL BARNETT INSPECTION SERVICES LTD </strong><p>10, Bordeaux Close, Northfield Green, SR3 2SR Sunderland - U.K.</p>");
		var popuppolonia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Poland / Ukraina c/o AdvaServe</strong><p>ul. Lanciego, 16/137 02-792 Warszawa - Poland </p>");
		var popupromania = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Romania / Czech Rep. c/o S.C. MIGRAL SRL </strong><p>Novai 5, Sector 5 - 051725- Bucharest - ROMANIA </p>");
		var popuptunisia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Tunisie / Algeria / Libya / Morocco c/o B A M Audit & Conseil SARL </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>");
		var popuparabia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Saudi Arabia c/o Mr. S. TAMIZHSELVAN, </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>");
		var popupsingapore = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE SINGAPORE c/o South East Asia Operations </strong><p>Singapore Branch Office Blk 473, Choa Chukang Avenue 3, # 13-165, APORE - 680473</p>");


		//array degli uffici
		var uffici = [
			{ nome: "Genova", lng: 8.8961465, lat: 44.4085366, popup: popupgenova },
			{ nome: "Cagliari", lng: 9.1606217, lat: 39.2008815, popup: popupcagliari },
			{ nome: "Boston", lng: -71.0572052, lat: 42.3530254, popup: popupboston },
			{ nome: "India", lng: 78.421020, lat: 17.488197, popup: popupindia },
			{ nome: "Cina", lng: 121.422842, lat: 31.217281, popup: popupcina },
			{ nome: "Uk", lng: -1.422084, lat: 54.867204, popup: popupuk },
			{ nome: "Polonia", lng: 21.056666, lat: 52.146997, popup: popuppolonia },
			{ nome: "Romania", lng: 26.070752, lat: 44.391045, popup: popupromania },
			{ nome: "Tunisia", lng: 10.179960, lat: 36.819510, popup: popuptunisia },
			{ nome: "Singapore", lng: 103.737832, lat: 1.379615, popup: popupsingapore },
			{ nome: "Arabia", lng: 50.053479, lat: 26.711185, popup: popuparabia },
		];

		for(var x = 0; x < uffici.length; x++){
			new mapboxgl.Marker({options: '#F0F0F0'})
				.setLngLat([uffici[x].lng, uffici[x].lat])
				.setPopup(uffici[x].popup)
				.addTo(map);
		}


		/*map.on('load', function () {
		 // Add a layer showing the places.
		 map.addLayer({
		 "id": "places",
		 "type": "symbol",

		 "source": {
		 "type": "geojson",
		 "data": {
		 "type": "FeatureCollection",
		 "features": [{
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>Genova</strong><p>Via Pietro Chiesa 9 - 3° piano - 16149 Genoa - ITALY</p>",
		 "icon": "suitcase",
		 "iconSize": [90, 90]
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [8.8961465, 44.4085366]

		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>Cagliari</strong><p>Via Ischia 4 - 09012 Capoterra - Cagliari - ITALY</p>",
		 "icon": "suitcase",
		 "iconSize": [50, 50]
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [9.1606217, 39.2008815],

		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>Boston</strong><p>125 Summer Street, Boston MA 02110 - USA</p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [-71.0572052, 42.3530254]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE INDIA c/o INTERNATIONAL INSPECTION SERVICES (P) LTD </strong><p>Plot No. 29, # 5-6-137/6/4/2, Behind Metro, <br>Kukatpally, Hyderabad - 500 072 INDIA</p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [78.421020, 17.488197]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE CHINA c/o THARSOS</strong><p>2701#, The South Tower, Huaninginternational plaza, No. 300 Xuanhua Road, <br>Changing Diostrict, Shanghai, CHINA</p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [121.422842, 31.217281]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE U.K. c/o NEIL BARNETT INSPECTION SERVICES LTD </strong><p>10, Bordeaux Close, Northfield Green, SR3 2SR Sunderland - U.K.</p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [-1.422084, 54.867204]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE Poland / Ukraina c/o AdvaServe</strong><p>ul. Lanciego, 16/137 02-792 Warszawa - Poland </p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [21.056666, 52.146997]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE Romania / Czech Rep. c/o S.C. MIGRAL SRL </strong><p>Novai 5, Sector 5 - 051725- Bucharest - ROMANIA </p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [26.070752, 44.391045]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE Tunisie / Algeria / Libya / Morocco c/o B A M Audit & Conseil SARL </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [10.179960, 36.819510 ]
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE Saudi Arabia c/o Mr. S. TAMIZHSELVAN, </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>",
		 //"icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [50.053479, 26.711185],
		 }
		 }, {
		 "type": "Feature",
		 "properties": {
		 "description": "<strong>TECHNO SERVICE SINGAPORE c/o South East Asia Operations </strong><p>Singapore Branch Office Blk 473, Choa Chukang Avenue 3, # 13-165, APORE - 680473</p>",
		 "icon": "suitcase"
		 },
		 "geometry": {
		 "type": "Point",
		 "coordinates": [103.737832, 1.379615]
		 }
		 }]
		 }
		 },
		 "layout": {
		 "icon-image": "{icon}-15",
		 "icon-allow-overlap": true
		 }
		 });

		 // When a click event occurs on a feature in the places layer, open a popup at the
		 // location of the feature, with description HTML from its properties.
		 map.on('click', 'places', function (e) {
		 var coordinates = e.features[0].geometry.coordinates.slice();
		 var description = e.features[0].properties.description;

		 // Ensure that if the map is zoomed out such that multiple
		 // copies of the feature are visible, the popup appears
		 // over the copy being pointed to.
		 while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
		 coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
		 }

		 new mapboxgl.Popup()
		 .setLngLat(coordinates)
		 .setHTML(description)
		 .addTo(map);


		 });

		 // Change the cursor to a pointer when the mouse is over the places layer.
		 map.on('mouseenter', 'places', function () {
		 map.getCanvas().style.cursor = 'pointer';
		 });

		 // Change it back to a pointer when it leaves.
		 map.on('mouseleave', 'places', function () {
		 map.getCanvas().style.cursor = '';
		 });
		 });*/




// 		var popupOne = new mapboxgl.Popup({ offset: 15 })
// 			.setText('Via Pietro Chiesa 9 - 3° piano - 16149 Genoa - ITALY');
// 		var popupTwo = new mapboxgl.Popup({ offset: 15 })
// 			.setText('Via Ischia 4 - 09012 Capoterra - Cagliari - ITALY');
// 		var popupThree = new mapboxgl.Popup({ offset: 15 })
// 			.setText('125 Summer Street, Boston MA 02110 - USA');
// 		var popupFour = new mapboxgl.Popup({ offset: 15 })
// 			.setText('Plot No. 29, # 5-6-137/6/4/2, Behind Metro, Sri Sai Nagar Colony, Near Sai Baba Temple, Kukatpally, Hyderabad - 500 072 INDIA ');
//
// // create the marker
// 		new mapboxgl.Marker()
// 			.setLngLat([8.8961465, 44.4085366])
// 			.setPopup(popupOne) // sets a popup on this marker
// 			.addTo(map);
//
// 		new mapboxgl.Marker()
// 			.setLngLat([9.1606217, 39.2008815])
// 			.setPopup(popupTwo) // sets a popup on this marker
// 			.addTo(map);


	}



//DIAGRAM PAGINA ABOUT
// 	if($('.diagram').length){
// 		const initialAngle = function(diagram) {
// 			let initial = 0;
// 			for (let j = 0; j < diagram.classList.length; j++) {
// 				initial += parseFloat((/diagram-angle\s*:\s*(.+?)deg/i.exec(diagram.classList[j]) || [ ])[1]) || 0;
// 			}
// 			return initial;
// 		};
//
// 		const diagramStars = function() {
// 			const diagrams = document.querySelectorAll(".diagram-star");
// 			for (let i = 0; i < diagrams.length; i++) {
// 				const diagram = diagrams[i];
// 				const canvas = document.createElement("canvas"),
// 					sections = diagram.querySelectorAll("section").length,
// 					width = diagram.offsetWidth * (window.devicePixelRatio || 1),
// 					height = diagram.offsetHeight * (window.devicePixelRatio || 1),
// 					initial = -90 + initialAngle(diagram);
//
// 				canvas.width = width;
// 				canvas.height = height;
// 				const ctx = canvas.getContext("2d");
//
// 				ctx.strokeStyle = "#9b9b9b";
// 				ctx.lineWidth = window.devicePixelRatio || 1;
// 				ctx.beginPath();
// 				for (let angle = 0; angle < 360; angle += 360 / sections) {
// 					// Draw a line from the mid point, to a point at a certain angle
// 					ctx.moveTo(width / 2, height / 2);
// 					ctx.lineTo(
// 						width / 2 + width / 2 * Math.cos((angle + initial) * Math.PI / 180),
// 						height / 2 + height / 2 * Math.sin((angle + initial) * Math.PI / 180));
// 				}
// 				ctx.stroke();
//
// 				diagram.style.backgroundImage = `url(${canvas.toDataURL("image/png")})`;
// 			}
// 		};
//
// 		const diagramPositions = function() {
// 			const alignment = function(angle) {
// 				if (angle < 0) { angle %= 360; }
// 				if (angle >= 360) { angle = angle % 360 + 360; }
//
// 				if (angle >= 330 || angle < 30) {
// 					return "top";
// 				}
// 				if (angle >= 150 && angle < 210) {
// 					return "bottom";
// 				}
// 				if (angle >= 30 && angle < 150) {
// 					return "right";
// 				}
// 				return "left";
// 			}
//
// 			const rem = parseInt(window.getComputedStyle(document.querySelector("html"), null).getPropertyValue("font-size"), 10);
// 			const diagrams = document.querySelectorAll(".diagram");
//
// 			for (let i = 0; i < diagrams.length; i++) {
// 				const diagram = diagrams[i],
// 					sections = diagram.querySelectorAll("section"),
// 					width = diagram.offsetWidth / rem,
// 					height = diagram.offsetHeight / rem,
// 					initial = -90 + initialAngle(diagram);
//
// 				for (let j = 0; j < sections.length; j++) {
// 					const section = sections[j],
// 						angle = 360 / sections.length * j + initial,
// 						x = width / 2 * Math.cos(angle * Math.PI / 180),
// 						y = height / 2 * Math.sin(angle * Math.PI / 180);
//
// 					section.style.left = `calc(50% + ${x}rem)`;
// 					section.style.top = `calc(50% + ${y}rem)`;
// 					section.classList.remove("alignment-top", "alignment-right", "alignment-bottom", "alignment-left");
// 					section.classList.add(`alignment-${alignment(angle + 90)}`);
// 				}
// 			}
// 		};
//
// // Replace this with ResizeObserver when the time is ready
// 		const watch = function(what, trigger) {
// 			let value = what();
// 			trigger();
//
// 			const check = function() {
// 				let current = what();
// 				if (current !== value) {
// 					trigger();
// 					value = current;
// 				}
// 				setTimeout(check, 100);
// 			};
//
// 			setTimeout(check, 100);
// 		};
//
// 		const diagrams = document.querySelectorAll(".diagram"),
// 			starDiagrams = document.querySelectorAll(".diagram-star");
//
// 		watch(
// 			function() {
// 				let result = 0;
// 				for (let i = 0; i < diagrams.length; i++) {
// 					result = result ^ diagrams[i].offsetWidth;
// 				}
// 				return result;
// 			},
// 			diagramPositions
// 		);
// 		watch(
// 			function() {
// 				let result = 0;
// 				for (let i = 0; i < starDiagrams.length; i++) {
// 					result = result ^ starDiagrams[i].offsetWidth;
// 				}
// 				return result;
// 			},
// 			diagramStars
// 		);
// 	}





});
