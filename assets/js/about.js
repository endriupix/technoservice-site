$(document).ready(function () {

//TIMELINE ABOUT
if($('#timeline-about-section').length){
	window.sr = ScrollReveal();

	if ($(window).width() < 768) {

		if ($('.timeline-content').hasClass('js--fadeInLeft')) {
			$('.timeline-content').removeClass('js--fadeInLeft').addClass('js--fadeInRight');
		}

		sr.reveal('.js--fadeInRight', {
			origin: 'right',
			distance: '300px',
			easing: 'ease-in-out',
			duration: 800,
		});

	} else {

		sr.reveal('.js--fadeInLeft', {
			origin: 'left',
			distance: '300px',
			easing: 'ease-in-out',
			duration: 800,
		});

		sr.reveal('.js--fadeInRight', {
			origin: 'right',
			distance: '300px',
			easing: 'ease-in-out',
			duration: 800,
		});

	}

	sr.reveal('.js--fadeInLeft', {
		origin: 'left',
		distance: '300px',
		easing: 'ease-in-out',
		duration: 800,
	});

	sr.reveal('.js--fadeInRight', {
		origin: 'right',
		distance: '300px',
		easing: 'ease-in-out',
		duration: 800,
	});
}


//Loghi partners carousel
	if($('.customer-logos').length){
		$('.customer-logos').slick({
			slidesToShow: 11,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2500,
			arrows: false,
			dots: false,
			pauseOnHover: false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 6
				}
			}, {
				breakpoint: 520,
				settings: {
					slidesToShow: 2
				}
			}]
		});
	};
});
