$(document).ready(function () {
//Loghi partners carousel
	if ($('.customer-logos').length) {
		$('.customer-logos').slick({
			slidesToShow: 11,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2500,
			arrows: false,
			dots: false,
			pauseOnHover: false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 6
				}
			}, {
				breakpoint: 520,
				settings: {
					slidesToShow: 1
				}
			}]
		});
	}
	;
});
