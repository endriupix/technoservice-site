$(document).ready(function () {
	if($('#map-home').length){
		mapboxgl.accessToken = 'pk.eyJ1IjoiZW5kcml1cGl4dGVjaG5vc2VydmljZSIsImEiOiJjanJtY2l3cngwMWFzNDNvMHdpYmg2OGRtIn0.yxwskhVCY8dV7FG3_BhlbQ';
		var map = new mapboxgl.Map({
			container: 'map-home', // container id
			style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
			center: [8.8961465, 44.4085366], // starting position [lng, lat]
			zoom: 3, // starting zoom
			sprite: "mapbox://sprites/mapbox/streets-v8",
			glyphs: "mapbox://fonts/mapbox/{fontstack}/{range}.pbf",
		});
		//map['scrollZoom'].disable();
		var popupgenova = new mapboxgl.Popup()
			.setHTML("<strong>Genova</strong><p>Via Pietro Chiesa 9 - 3° piano - 16149 Genoa - ITALY</p>");
		var popupcagliari = new mapboxgl.Popup()
			.setHTML("<strong>Cagliari</strong><p>Via Ischia 4 - 09012 Capoterra - Cagliari - ITALY</p>");
		var popupboston = new mapboxgl.Popup()
			.setHTML("<strong>Boston</strong><p>125 Summer Street, Boston MA 02110 - USA</p>");
		var popupindia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE INDIA c/o INTERNATIONAL INSPECTION SERVICES (P) LTD </strong><p>Plot No. 29, # 5-6-137/6/4/2, Behind Metro, <br>Kukatpally, Hyderabad - 500 072 INDIA</p>");
		var popupcina = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE CHINA c/o THARSOS</strong><p>2701#, The South Tower, Huaninginternational plaza, No. 300 Xuanhua Road, <br>Changing Diostrict, Shanghai, CHINA</p>");
		var popupuk = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE U.K. c/o NEIL BARNETT INSPECTION SERVICES LTD </strong><p>10, Bordeaux Close, Northfield Green, SR3 2SR Sunderland - U.K.</p>");
		var popuppolonia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Poland / Ukraina c/o AdvaServe</strong><p>ul. Lanciego, 16/137 02-792 Warszawa - Poland </p>");
		var popupromania = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Romania / Czech Rep. c/o S.C. MIGRAL SRL </strong><p>Novai 5, Sector 5 - 051725- Bucharest - ROMANIA </p>");
		var popuptunisia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Tunisie / Algeria / Libya / Morocco c/o B A M Audit & Conseil SARL </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>");
		var popuparabia = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE Saudi Arabia c/o Mr. S. TAMIZHSELVAN, </strong><p>9 Rue de Jérusalem, 1002, Tunis - Tunisie </p>");
		var popupsingapore = new mapboxgl.Popup()
			.setHTML("<strong>TECHNO SERVICE SINGAPORE c/o South East Asia Operations </strong><p>Singapore Branch Office Blk 473, Choa Chukang Avenue 3, # 13-165, APORE - 680473</p>");


		//array degli uffici
		var uffici = [
			{ nome: "Genova", lng: 8.8961465, lat: 44.4085366, popup: popupgenova },
			{ nome: "Cagliari", lng: 9.1606217, lat: 39.2008815, popup: popupcagliari },
			{ nome: "Boston", lng: -71.0572052, lat: 42.3530254, popup: popupboston },
			{ nome: "India", lng: 78.421020, lat: 17.488197, popup: popupindia },
			{ nome: "Cina", lng: 121.422842, lat: 31.217281, popup: popupcina },
			{ nome: "Uk", lng: -1.422084, lat: 54.867204, popup: popupuk },
			{ nome: "Polonia", lng: 21.056666, lat: 52.146997, popup: popuppolonia },
			{ nome: "Romania", lng: 26.070752, lat: 44.391045, popup: popupromania },
			{ nome: "Tunisia", lng: 10.179960, lat: 36.819510, popup: popuptunisia },
			{ nome: "Singapore", lng: 103.737832, lat: 1.379615, popup: popupsingapore },
			{ nome: "Arabia", lng: 50.053479, lat: 26.711185, popup: popuparabia },
		];

		for(var x = 0; x < uffici.length; x++){
			new mapboxgl.Marker({options: '#F0F0F0'})
				.setLngLat([uffici[x].lng, uffici[x].lat])
				.setPopup(uffici[x].popup)
				.addTo(map);
		}



	}



//Loghi partners carousel
if($('.customer-logos').length){
	$('.customer-logos').slick({
		slidesToShow: 7,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2500,
		arrows: false,
		dots: false,
		pauseOnHover: false,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2
			}
		}]
	});
};

});
