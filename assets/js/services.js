$(document).ready(function () {
	switchClassContainerServices();


window.onresize = function(event) {
	switchClassContainerServices();
};


function switchClassContainerServices() {
	if(window.innerWidth < 1000){
		$("#service-item-container").removeClass("container");
		$("#service-item-container").addClass("container-fluid");
	}else{
		$("#service-item-container").removeClass("container-fluid");
		$("#service-item-container").addClass("container");
	}
};
//Sotto i 1000px passo al container fluid


$('a.btn-gallery').on('click', function(event) {
	event.preventDefault();

	var gallery = $(this).attr('href');

	$(gallery).magnificPopup({
		delegate: 'a',
		type:'image',
		gallery: {
			enabled: true
		}
	}).magnificPopup('open');
});
});
