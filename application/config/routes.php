<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages/viewhome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['about'] = 'pages/viewabout';
$route['en/abouteng'] = 'pages/viewabout';
$route['services'] = 'pages/viewservices';
$route['en/serviceseng'] = 'pages/viewservices';
$route['work'] = 'pages/viewwork';
$route['en/workeng'] = 'pages/viewwork';
$route['clients'] = 'pages/viewclients';
$route['en/clientseng'] = 'pages/viewclients';
$route['contacts'] = 'pages/viewcontacts';
$route['en/contactseng'] = 'pages/viewcontacts';
$route['privacy'] = 'pages/viewprivacy';
$route['en/privacy'] = 'pages/viewprivacy';
$route['cookiepolicy'] = 'pages/viewcookiepolicy';
$route['en/cookiepolicy'] = 'pages/viewcookiepolicy';




// URI like '/en/about' -> use controller 'about'
//$route['^(en|de|fr|it)/(.+)$'] = 'pages/view$2';
// $route['^(en|it)/contact'] = "pages/contact";
//$route['^(en|it)/privacy-policy$'] = "pages/index/privacy_policy";
//$route['^(en|it)/terms-of-use$'] = "pages/index/terms_of_use";
// '/en', '/de', '/fr'  URIs -> use default controller
$route['^(en|it)$'] = $route['default_controller'];




//$route['(:any)'] = $route['default_controller'];
//$route['^(en|it)/services'] = "pages/viewservices";
