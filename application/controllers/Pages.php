<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Controlla se c'è la sigla di una lingua nell'url
	 * carica i file corretti e crea un array con tutte le
	 * variabili necessarie alla view
	 *
	 * @return	array
	 */
	private function initializeController($page){

		$this->load->helper(array('url','html'));
		$this->load->helper('language');
//		$this->load->library('mybreadcrumb');

		$data_lang = array();

		$routes_multilanguage['routes_translation']['en'] = array(
			'home' => 'en/',
			'about' => 'en/abouteng',
			'clients' => 'en/clientseng',
			'contacts' => 'en/contactseng',
			'services' => 'en/serviceseng',
			'work' => 'en/workeng',
			'privacy' => 'en/privacy',
			'cookiepolicy' => 'en/cookiepolicy',
		);

		$routes_multilanguage['routes_translation']['it'] = array(
			'home' => '',
			'about' => 'about',
			'clients' => 'clients',
			'contacts' => 'contacts',
			'services' => 'services',
			'work' => 'work',
			'privacy' => 'privacy',
			'cookiepolicy' => 'cookiepolicy',
		);

		if(isset($this->uri->segments[1])){$lang= $this->uri->segments[1];}else{$lang = "it";}
		if ($lang == "en"){
			// load english contact view
			$this->lang->load('pages', 'english');
			$this->lang->load('routes', 'english');
			$this->lang->load('images', 'english');
			$this->lang->load('seo', 'english');
			$this->lang->load('strings', 'english');
			$this->lang->load('privacy', 'english');
			$data_lang["lang_abbr"] = $lang;
		}else{
			$this->lang->load('pages', 'italian');
			$this->lang->load('routes', 'italian');
			$this->lang->load('images', 'italian');
			$this->lang->load('seo', 'italian');
			$this->lang->load('strings', 'italian');
			$this->lang->load('privacy', 'italian');
			$data_lang["lang_abbr"] = 'it';
		}

		$data_lang["title"] = $this->lang->line("seo_" . $page . "_title");
		$data_lang["lang_flag_en"] = 'flag-bandiera-lingua-inglese-en.png';
		$data_lang["lang_flag_it"] = 'flag-bandiera-lingua-italiana-italia.png';
		$data_lang["canonical"] = site_url() . $routes_multilanguage['routes_translation'][$data_lang["lang_abbr"]][$page];
		$data_lang["hreflang_en"] = site_url() . $routes_multilanguage['routes_translation']['en'][$page];
		$data_lang["hreflang_it"] = site_url() . $routes_multilanguage['routes_translation']['it'][$page];
		$data_lang["x_default"] = site_url() . $routes_multilanguage['routes_translation']['it'][$page];

		return $data_lang;
	}

	/**
	 * Restituisce utti i servizi
	 *
	 * @return	array
	 */
	private function getServices(){
		$all_services[0] = array(
			'id' => '001',
			'image' => '/img/Petrolchimico-raffinazione.jpg',
			'link' => '/img/Petrolchimico-raffinazione.jpg',
			'linkgallery' => '#gallery-1',
			'nome' => $this->lang->line('page_service_oil_gas'),
			'descrizione' => $this->lang->line('page_service_oil_gas_descr'),
			'class' => 'oil_and_gas',
			'icon' => 'fas fa-oil-can',
			'children' => array(
				0  => $this->lang->line('page_service_onoffshore'),
				1  => $this->lang->line('page_service_piattaforma'),
				2  => $this->lang->line('page_service_barge'),
				3  => $this->lang->line('page_service_pipeline'),
				4  => $this->lang->line('page_service_rivestimenti'),
				6  => $this->lang->line('page_service_trattamentogas'),
			)
		);
		$all_services[1] = array(
			'id' => '002',
			'image' => '/img/stock-photo-solar-panels-with-wind-turbines-and-electricity-pylon-at-sunset-clean-energy-concept-689040328Res.jpg',
			'link' => '/img/stock-photo-solar-panels-with-wind-turbines-and-electricity-pylon-at-sunset-clean-energy-concept-689040328Res.jpg',
			'linkgallery' => '#gallery-2',
			'nome' => $this->lang->line('page_service_energia'),
			'descrizione' => $this->lang->line('page_service_energia_descr'),
			'class' => 'energia',
			'icon' => 'fas fa-bolt',
			'children' => array(
				0  => $this->lang->line('page_service_centralielettriche'),
				1  => $this->lang->line('page_service_centralinucleari'),
				2  => $this->lang->line('page_service_sottostazioni'),
				3  => $this->lang->line('page_service_pipeline'),
				4  => $this->lang->line('page_service_eolico'),
				6  => $this->lang->line('page_service_fotovoltaico')
			)
		);

		$all_services[2] = array(
			'id' => '003',
			'image' => '/img/stock-photo-metalworking-factory-roll-of-galvanized-steel-sheet-for-manufacturing-metal-pipes-and-tubes-in-the-779798491Res.jpg',
			'link' => '/img/stock-photo-metalworking-factory-roll-of-galvanized-steel-sheet-for-manufacturing-metal-pipes-and-tubes-in-the-779798491Res.jpg',
			'linkgallery' => '#gallery-3',
			'nome' => $this->lang->line('page_service_siderurgia'),
			'descrizione' => $this->lang->line('page_service_siderurgia_descr'),
			'class' => 'siderurgia',
			'icon' => 'fas fa-industry',
			'children' => array(
				0  => $this->lang->line('page_service_acciaieria'),
				1  => $this->lang->line('page_service_primario'),
				2  => $this->lang->line('page_service_laminazione'),
				3  => $this->lang->line('page_service_tubificio')
			)
		);

		$all_services[3] = array(
			'id' => '004',
			'image' => '/img/stock-photo-interior-of-modern-advanced-technology-vessel-dynamic-positioning-ship-620042147Res.png',
			'link' => 'https://placeholder.com',
			'linkgallery' => '#gallery-4',
			'nome' => $this->lang->line('page_service_navale'),
			'descrizione' => $this->lang->line('page_service_navale_descr'),
			'class' => 'navale',
			'icon' => 'fas fa-ship',
			'children' => array(
				0  => $this->lang->line('page_service_componenti'),
				1  => $this->lang->line('page_service_barge')
			)
		);
		$all_services[4] = array(
			'id' => '005',
			'image' => '/img/stock-photo-refinery-oil-and-gas-industry-533134600Res.png',
			'link' => 'https://placeholder.com',
			'linkgallery' => '#gallery-5',
			'nome' => $this->lang->line('page_service_chimico'),
			'descrizione' => $this->lang->line('page_service_chimico_descr'),
			'class' => 'chimico',
			'icon' => 'fas fa-vial',
			'children' => array(
				0  => $this->lang->line('page_service_ammoniaurea'),
				1  => $this->lang->line('page_service_polimeri'),
				2  => $this->lang->line('page_service_impiantochimico')
			)
		);
		$all_services[5] = array(
			'id' => '006',
			'image' => '/img/Trattamento-acque-dissalazione.jpg',
			'link' => 'https://placeholder.com',
			'linkgallery' => '#gallery-6',
			'nome' => $this->lang->line('page_service_ecologia'),
			'descrizione' => $this->lang->line('page_service_ecologia_descr'),
			'class' => 'ecologia',
			'icon' => 'fab fa-canadian-maple-leaf',
			'children' => array(
				0  => $this->lang->line('page_service_trattamentoacqua'),
				1  => $this->lang->line('page_service_dissalatori'),
				2  => $this->lang->line('page_service_trattamentorifiuti')
			)
		);
		$all_services[6] = array(
			'id' => '007',
			'image' => '/img/stock-photo-large-tonnage-industrial-orange-goliath-crane-loading-of-goods-in-factory-mill-50100277Res.png',
			'link' => 'https://placeholder.com',
			'linkgallery' => '#gallery-7',
			'nome' => $this->lang->line('page_service_sollevamento'),
			'descrizione' => $this->lang->line('page_service_sollevamento_descr'),
			'class' => 'sollevamento',
			'icon' => 'fas fa-snowplow',
			'children' => array(
				0  => $this->lang->line('page_service_grubraccio'),
				1  => $this->lang->line('page_service_grucavalletto'),
				2  => $this->lang->line('page_service_scaricatoricontainer'),
				3  => $this->lang->line('page_service_grubanchina'),
				4  => $this->lang->line('page_service_ferratigommati'),
				6  => $this->lang->line('page_service_carriponte'),
			)
		);

		return $all_services;
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function viewhome($page = 'home')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		//$data_language = $this->initializeController($page);
		$data['title'] = ucfirst($page); // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $this->initializeController($page);
		$data['servizi'] = $this->getServices();

		$data['call_to_actions'] = array(
			0 => array(
				'cta_title' => $this->lang->line('page_home_cta_title_whatwedo'),
				'cta_text' => $this->lang->line('page_home_cta_text_whatwedo'),
				'cta_button_text' => $this->lang->line('page_home_cta_button_text_whatwedo'),
				'cta_button_url' => $this->lang->line('route_services'),
				'cta_button_url_title' => $this->lang->line('page_home_cta_button_url_title_whatwedo'),
			),
			1 => array(
				'cta_title' => $this->lang->line('page_home_cta_title_workwithus'),
				'cta_text' => $this->lang->line('page_home_cta_text_workwithus'),
				'cta_button_text' => $this->lang->line('page_home_cta_button_text_workwithus'),
				'cta_button_url' => $this->lang->line('route_work'),
				'cta_button_url_title' => $this->lang->line('page_home_cta_button_url_title_workwithus'),
			),
		);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewabout($page = 'about')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = $this->lang->line('seo_about_title_page');; // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;
		$data['call_to_actions'] = array(
			0 => array(
				'cta_title' => $this->lang->line('page_home_cta_title_whatwedo'),
				'cta_text' => $this->lang->line('page_home_cta_text_whatwedo'),
				'cta_button_text' => $this->lang->line('page_home_cta_button_text_whatwedo'),
				'cta_button_url' => $this->lang->line('route_services'),
				'cta_button_url_title' => $this->lang->line('page_home_cta_button_url_title_whatwedo'),
			),
		);

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_about_title'), $this->lang->line('route_about'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));
		// populate multiple
//			$this->breadcrumb->populate(array(
//				'About' => $routes_multilanguage['routes_translation']['en']["about"],
//				'All'
//			));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewservices($page = 'services')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = $this->lang->line('seo_services_title_page'); // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;
		$data['servizi'] = $this->getServices();

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_services_title'), $this->lang->line('route_services'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewwork($page = 'work')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = $this->lang->line('seo_work_title_page');; // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_work_title'), $this->lang->line('route_work'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewclients($page = 'clients')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = $this->lang->line('seo_clients_title_page'); // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_clients_title'), $this->lang->line('route_clients'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewcontacts($page = 'contacts')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = $this->lang->line('seo_contacts_title_page'); // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_contacts_title'), $this->lang->line('route_contacts'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewprivacy($page = 'privacy')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = ucfirst($page); // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_privacy_title'), $this->lang->line('route_privacy'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function viewcookiepolicy($page = 'cookiepolicy')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data_language = $this->initializeController($page);
		$data['title'] = "Cookie Policy"; // Capitalize the first letter
		$data['page'] = $page; // Capitalize the first letter
		$data['data_language'] = $data_language;

		/***BREADCRUMB*/
		$this->breadcrumb->append($this->lang->line('seo_cookiepolicy_title'), $this->lang->line('route_cookiepolicy'));
		// prepend breadcrumbs, link parameter is optional
		$this->breadcrumb->prepend($this->lang->line('seo_home_title'), $this->lang->line('route_home'));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('templates/page-header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
}
