<?php
$lang['string_contact_us'] = 'Contattaci';
$lang['string_about'] = 'Chi Siamo';
$lang['string_our_services'] = 'I Nostri Servizi';
$lang['string_our_clients'] = 'I Nostri Clienti';
$lang['string_service_ispezione'] = 'Ispezione';
$lang['string_service_collaudo'] = 'Collaudo';
$lang['string_service_survey'] = 'Survey';
$lang['string_service_expediting'] = 'Expediting ';
$lang['string_service_button_other'] = 'Tutti i Servizi';
$lang['string_service_button_scopri'] = 'Scopri i Servizi';
$lang['string_realta_ts'] = 'La realtà Techno Service';
$lang['string_inviare_candidatura'] = 'Invia candidatura e cv a ';
$lang['string_read_more'] = 'Read More';
$lang['string_links_utili'] = 'Links Utili';
$lang['string_gallery'] = 'Galleria';
$lang['string_direzione'] = 'Direzione';
$lang['string_amministrazione'] = 'Amministrazione';
$lang['string_viewphoto'] = 'Guarda le Foto';

