<?php
$lang['privacy_text_page'] = '
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies justo a varius rhoncus. Cras scelerisque, 
nunc vel efficitur viverra, nisi risus posuere arcu, eu convallis sapien eros non neque. Maecenas maximus, turpis 
eget suscipit ultricies, nisl nisl tempus est, ut consectetur diam nisi vitae nisl. Curabitur vestibulum venenatis 
imperdiet. Phasellus et tempor tortor. Proin sed mi sit amet elit semper pulvinar sed non nisi. Ut interdum elit in 
aliquet gravida. Phasellus sed vehicula nulla, vitae gravida purus. Nam a ante non sapien volutpat aliquam. 
Suspendisse metus arcu, malesuada sit amet eros id, dictum eleifend neque. Orci varius natoque penatibus et magnis 
dis parturient montes, nascetur ridiculus mus. Praesent ullamcorper vulputate gravida. Sed maximus enim sed augue 
tincidunt, vel congue felis tincidunt. Curabitur maximus quis lorem quis aliquam.
</p>
<p>
Suspendisse pretium lacus eget fermentum interdum. Curabitur scelerisque porta nibh. Vestibulum semper purus sit amet 
consequat tempor. Vestibulum justo velit, elementum nec accumsan et, tristique nec massa. Nam ut tempor arcu, sit amet 
rutrum erat. Aenean eros justo, accumsan faucibus tellus eget, finibus efficitur erat. Cras mattis lacinia orci, a 
faucibus ex faucibus sed. Sed egestas velit sit amet purus ultricies, ac ornare quam eleifend. Donec finibus porta odio 
non porttitor.
</p>
<p>
Donec dignissim neque eget molestie placerat. Fusce gravida, ligula nec venenatis condimentum, dolor diam rutrum erat, 
id venenatis ligula dolor consequat magna. Pellentesque id erat tristique, semper erat id, elementum metus. Pellentesque 
gravida at neque eget laoreet. Curabitur et ultricies massa. Aenean in faucibus tellus, quis tempus nisi. Quisque vitae
 aliquet lorem. Proin tincidunt ex in sem elementum, quis tempus lorem varius. Mauris ultricies libero vel ex iaculis 
 hendrerit. Ut vel quam pulvinar, mollis ligula sed, convallis est. Aliquam vitae nisl vitae ipsum molestie malesuada 
 vitae et ligula. Praesent sodales, ligula quis luctus fringilla, odio diam posuere justo, vitae aliquam nibh enim 
 consectetur leo. Sed sollicitudin massa arcu, eget tincidunt leo viverra et. Praesent at arcu felis. Sed ipsum tortor, 
 vulputate id commodo nec, rhoncus in ipsum. Praesent ut quam at nulla dignissim fringilla at in eros.
</p>
<p>
Vivamus lobortis porttitor turpis, ac pharetra ex convallis in. Pellentesque habitant morbi tristique senectus et 
netus et malesuada fames ac turpis egestas. Duis pulvinar bibendum odio, congue convallis nibh interdum non. Curabitur
 venenatis pharetra sem, nec feugiat tellus vestibulum vitae. Nulla facilisi. Orci varius natoque penatibus et magnis 
 dis parturient montes, nascetur ridiculus mus. Mauris dignissim nisi ipsum, at viverra enim tempus vel. Sed vitae magna 
 sed est auctor varius malesuada a leo. Mauris urna tellus, dapibus eu placerat ac, faucibus et enim. Vestibulum dui sem,
  congue id nulla quis, venenatis viverra libero. Donec sed sodales sapien, ac convallis neque.
</p>
<p>
Vestibulum efficitur magna convallis, scelerisque arcu vitae, mollis neque. Integer eu quam quis est consequat
 pharetra at eget lorem. Integer lacinia sem nec massa congue imperdiet. Quisque nec libero congue, facilisis dolor
  in, sagittis lacus. In vehicula, ante vel blandit molestie, metus felis ullamcorper orci, eu aliquam felis velit 
  eu ipsum. Duis tempus efficitur mauris, id interdum ante lobortis a. Phasellus at accumsan ipsum. Nulla cursus diam
   nec tristique porttitor. Integer ex ligula, ullamcorper sit amet nunc et, condimentum laoreet felis. Cras efficitur 
   nulla ut ultricies ultrices. Maecenas lorem turpis, lobortis sed volutpat nec, lacinia non arcu. Aenean tincidunt 
   odio nibh, finibus consequat urna mattis ac.
</p>';
$lang['privacy_cookiepolicy_text_page'] = '
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies justo a varius rhoncus. Cras scelerisque, 
nunc vel efficitur viverra, nisi risus posuere arcu, eu convallis sapien eros non neque. Maecenas maximus, turpis 
eget suscipit ultricies, nisl nisl tempus est, ut consectetur diam nisi vitae nisl. Curabitur vestibulum venenatis 
imperdiet. Phasellus et tempor tortor. Proin sed mi sit amet elit semper pulvinar sed non nisi. Ut interdum elit in 
aliquet gravida. Phasellus sed vehicula nulla, vitae gravida purus. Nam a ante non sapien volutpat aliquam. 
Suspendisse metus arcu, malesuada sit amet eros id, dictum eleifend neque. Orci varius natoque penatibus et magnis 
dis parturient montes, nascetur ridiculus mus. Praesent ullamcorper vulputate gravida. Sed maximus enim sed augue 
tincidunt, vel congue felis tincidunt. Curabitur maximus quis lorem quis aliquam.
</p>
<p>
Suspendisse pretium lacus eget fermentum interdum. Curabitur scelerisque porta nibh. Vestibulum semper purus sit amet 
consequat tempor. Vestibulum justo velit, elementum nec accumsan et, tristique nec massa. Nam ut tempor arcu, sit amet 
rutrum erat. Aenean eros justo, accumsan faucibus tellus eget, finibus efficitur erat. Cras mattis lacinia orci, a 
faucibus ex faucibus sed. Sed egestas velit sit amet purus ultricies, ac ornare quam eleifend. Donec finibus porta odio 
non porttitor.
</p>
<p>
Donec dignissim neque eget molestie placerat. Fusce gravida, ligula nec venenatis condimentum, dolor diam rutrum erat, 
id venenatis ligula dolor consequat magna. Pellentesque id erat tristique, semper erat id, elementum metus. Pellentesque 
gravida at neque eget laoreet. Curabitur et ultricies massa. Aenean in faucibus tellus, quis tempus nisi. Quisque vitae
 aliquet lorem. Proin tincidunt ex in sem elementum, quis tempus lorem varius. Mauris ultricies libero vel ex iaculis 
 hendrerit. Ut vel quam pulvinar, mollis ligula sed, convallis est. Aliquam vitae nisl vitae ipsum molestie malesuada 
 vitae et ligula. Praesent sodales, ligula quis luctus fringilla, odio diam posuere justo, vitae aliquam nibh enim 
 consectetur leo. Sed sollicitudin massa arcu, eget tincidunt leo viverra et. Praesent at arcu felis. Sed ipsum tortor, 
 vulputate id commodo nec, rhoncus in ipsum. Praesent ut quam at nulla dignissim fringilla at in eros.
</p>
<p>
Vivamus lobortis porttitor turpis, ac pharetra ex convallis in. Pellentesque habitant morbi tristique senectus et 
netus et malesuada fames ac turpis egestas. Duis pulvinar bibendum odio, congue convallis nibh interdum non. Curabitur
 venenatis pharetra sem, nec feugiat tellus vestibulum vitae. Nulla facilisi. Orci varius natoque penatibus et magnis 
 dis parturient montes, nascetur ridiculus mus. Mauris dignissim nisi ipsum, at viverra enim tempus vel. Sed vitae magna 
 sed est auctor varius malesuada a leo. Mauris urna tellus, dapibus eu placerat ac, faucibus et enim. Vestibulum dui sem,
  congue id nulla quis, venenatis viverra libero. Donec sed sodales sapien, ac convallis neque.
</p>
<p>
Vestibulum efficitur magna convallis, scelerisque arcu vitae, mollis neque. Integer eu quam quis est consequat
 pharetra at eget lorem. Integer lacinia sem nec massa congue imperdiet. Quisque nec libero congue, facilisis dolor
  in, sagittis lacus. In vehicula, ante vel blandit molestie, metus felis ullamcorper orci, eu aliquam felis velit 
  eu ipsum. Duis tempus efficitur mauris, id interdum ante lobortis a. Phasellus at accumsan ipsum. Nulla cursus diam
   nec tristique porttitor. Integer ex ligula, ullamcorper sit amet nunc et, condimentum laoreet felis. Cras efficitur 
   nulla ut ultricies ultrices. Maecenas lorem turpis, lobortis sed volutpat nec, lacinia non arcu. Aenean tincidunt 
   odio nibh, finibus consequat urna mattis ac.
</p>';
