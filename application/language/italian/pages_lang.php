<?php

/****************HOMEPAGE**********************************/
$lang['page_home_tagline'] = 'Dal 1984 svolgiamo i nostri servizi a livello mondiale per conto di aziende leader, in Italia e nel mondo';
$lang['page_home_intro_title'] = '<span class="text-rosso">Controllo qualità</span> - Indagini - 
<span class="text-rosso">Inspection Testing</span> - Expediting';
$lang['page_home_intro_text'] = 'Dal 2010 Techno Service è stata certificata dall’Istituto Italiano della Saldatura (I.I.S.), 
primario organismo di certificazione, in conformità ai requisiti UNI EN ISO 9001:2015';
$lang['page_home_quality_title'] = 'La Qualità al servizio del cliente';
$lang['page_home_quality_text'] = '<p>TECHNO SERVICE intraprende da anni un percorso rivolto alla razionalizzazione ed al miglioramento dei processi interni
ed alla soddisfazione delle esigenze del Cliente.</p><p>Al fine di perseguire uno sviluppo costante, 
TECHNO SERVICE attribuisce la massima importanza alla continua implementazione ed al miglioramento del proprio Sistema 
di Gestione della Qualità nell\'ottica di soddisfare i seguenti principi qualitativi</p>';
$lang['page_home_services_title'] = 'I Servizi TecnoService';
$lang['page_home_services_text'] = '<p>TECHNO SERVICE offre servizi di assistenza tecnica specializzata, audits ed attività di survey, 
volti a garantire la Qualità nel pieno rispetto dei processi e dei sistemi di gestione.</p><p>Attraverso le proprie competenze, 
capacità decisionali, problem solving e professionalità, TECHNO SERVICE assiste il Cliente 
nel raggiungimento dei propri obiettivi, avvalendosi di personale qualificato in grado di fornire un supporto concreto nelle 
varie fasi di sviluppo di ciascun progetto e nell\' erogazione di servizi di verifica e supervisione tecnica.</p>';
$lang['page_home_map_title'] = 'TecnoService nel Mondo';
$lang['page_home_cta_title'] = '';
$lang['page_home_cta_text'] = '';
$lang['page_home_cta_button_text'] = 'Cosa Facciamo';

$lang['page_home_cta_title_whatwedo'] = '';
$lang['page_home_cta_text_whatwedo'] = '<p><span class="font-weight-bold">Dal 1984 TechnoService è al servizio dei suoi clienti. 
<br>Monitoriamo </span><span class="font-italic">costantemente</span> <span class="font-weight-bold">la nostra struttura per </span>
<span class="font-italic">garantire</span> <span class="font-weight-bold">questa qualità</span></p>';
$lang['page_home_cta_button_text_whatwedo'] = 'Cosa Facciamo';
$lang['page_home_cta_button_url_title_whatwedo'] = '';
$lang['page_home_cta_title_workwithus'] = '';
$lang['page_home_cta_text_workwithus'] = '<p>Siamo orgogliosi del lavoro che facciamo e non avremmo successo senza il nostro fantastico team di persone dedicate. Vuoi far parte del nostro team?</p>';
$lang['page_home_cta_button_text_workwithus'] = 'Work With Us';
$lang['page_home_cta_button_url_title_workwithus'] = '';

/****************ABOUT**********************************/
$lang['page_about_intro_text'] = 'TECHNO SERVICE svolge le seguenti attività impiegando tecnici di comprovata esperienza 
nei seguenti settori industriali:<br>Meccanico - Elettrico - Strumentazione - Automazione - Civile - Refrattario';
$lang['page_about_timeline_title_one'] = 'Techno Service, una realtà dal 1984';
$lang['page_about_timeline_body_one'] = '<p>Sin dal 1984 TECHNO SERVICE svolge servizi di:</p>
<ul class=""><li class="">Quality control</li><li class="">Survey</li><li class="">Inspection</li>
<li class="">Testing</li><li class="">Expediting</li></ul><p>Questi servizi sono svolti da Techno Service a livello mondiale
per conto di primarie aziende operanti in Italia e nel Mondo.</p>';
$lang['page_about_timeline_date_one'] = 'Dal 1984';
$lang['page_about_timeline_title_two'] = 'I servizi di supervisione';
$lang['page_about_timeline_body_two'] = '<p>TECHNO SERVICE sin dal 1990 ha sviluppato ed ampliato il proprio 
core business a livello internazionale, nel	settore dei servizi di supervisore in cantiere quali:</p>
<ul class=""><li class="">QA/QC Management</li><li class="">Supervisione della Qualità in cantiere</li>
<li class="">Assistenza al Montaggio</li><li class="">Pre-Commissioning</li><li class="">Avviamento</li>
<li class="">Messa in Servizio</li></ul>';
$lang['page_about_timeline_date_two'] = 'Dal 1990';
$lang['page_about_timeline_title_three'] = 'Certificazione UNI EN ISO 9001:2015';
$lang['page_about_timeline_body_three'] = '<p>Dal 2010 TECHNO SERVICE è stata certificata dall\' Istituto Italiano della Saldatura 
(I.I.S.), primario organismo di certificazione, in conformit&agrave; ai requisiti UNI EN ISO 9001:2015&nbsp; 
<a href="/documents/Certificato_355_UNI_EN_ISO_9001_2015.pdf" target="_new">(Certificato No. 355 allegato)</a>.</p>';
$lang['page_about_tecnici_qualificati_title'] = 'Tecnici Qualificati';
$lang['page_about_tecnici_qualificati_body'] = '<p>I nostri Tecnici, che si occupano delle procedure dei Controlli 
Non Distruttivi, sono in possesso dei LIVELLI II e III in conformità agli standard  CIC PND nazionali 
ed a quelli internazionali ASNT TC 1A per i metodi:</p>
<ul class=""><li class="">VT  (Visivo)</li><li class="">UT (Ultrasonoro)</li><li class="">PT (Liquidi Penetranti)</li>
<li class="">RT (Radiografico)</li><li class="">MT (Magnetoscopico)</li></ul>
<p>Ispettori di saldatura qualificati operano nel rispetto degli standards nazionali ed internazionali(qualificati IIW).</p>
<p>Il personale tecnico include anche ispettori qualificati ARAMCO, sia per le attività di ispezione presso il fornitore, 
sia per le attività in cantiere. </p>
<p>Tutto il personale TECHNO SERVICE opera con le coperture assicurative di legge ed in ottemperanza 
alle disposizioni di legge previste in materia di sicurezza (legge 81/2008) ed alle disposizioni 
di legge locali previste nel Paese in cui il ns. personale andr&agrave; ad operare.</p>';
$lang['page_about_politica_qualita_title'] = 'Techno Service - La Politica per la Qualità';
$lang['page_about_politica_qualita_body'] = '<p>TECHNO SERVICE intraprende da anni un percorso rivolto alla razionalizzazione
	ed al miglioramento dei processi interni ed alla soddisfazione delle esigenze del Cliente.</p>
<p>Il modello  organizzativo e gestionale della Società, conforme alla norma UNI EN ISO 9001,
	ha consentito di ottenere la <a href="../../doc/Certificato_355.pdf" target="_new"> certificazione UNI EN ISO 9001:2015</a>
	rilasciata dall\' Istituto Italiano della Saldatura.</p>
<p>Al fine di perseguire uno sviluppo costante,  TECHNO SERVICE attribuisce la massima importanza
	alla continua implementazione ed al miglioramento del proprio Sistema di Gestione della Qualità
	nell\'ottica di soddisfare i seguenti principi qualitativi:</p>
<ul><li>organizzazione orientata al Cliente </li><li>leadership </li><li>coinvolgimento del personale </li>
<li>miglioramento continuo </li><li>iniziative basate su elementi attendibili e concreti </li></ul>
<p>La ns. Società ritiene altresì importante l\' adeguamento, lo sviluppo e l\' applicazione,
	da parte di tutto il personale, dei criteri del  Sistema Gestione Qualità
	riferiti alla norma UNI EN ISO 9001:2015
	ed il raggiungimento degli obiettivi garantendo la totale soddisfazione del Cliente.</p>
<p>A tale scopo viene richiesto e monitorato un costante sostegno ed un continuo coinvolgimento di
	tutta la struttura TECHNO SERVICE.</p>';


/****************SERVICES**********************************/
$lang['page_service_intro'] = '<p>TECHNO SERVICE offre servizi di assistenza tecnica specializzata, audits ed attività
di survey, volti a garantire la Qualità nel pieno rispetto dei processi e dei sistemi di gestione.
</p><p>Attraverso le proprie competenze, capacità decisionali, problem solving e professionalità,
TECHNO SERVICE assiste il Cliente nel raggiungimento dei propri obiettivi, avvalendosi di personale qualificato
in grado di fornire un supporto concreto nelle varie fasi di sviluppo di ciascun progetto e
nell\' erogazione di servizi di verifica e supervisione tecnica.</p>';

$lang['page_service_oil_gas'] = 'Petrolifero';
$lang['page_service_oil_gas_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_onoffshore'] = 'On/Offshore';
$lang['page_service_piattaforma'] = 'Piattaforma';
$lang['page_service_barge'] = 'Barge';
$lang['page_service_pipeline'] = 'Pipeline';
$lang['page_service_rivestimenti'] = 'Rivestimenti';
$lang['page_service_raffineria'] = 'Raffineria';
$lang['page_service_trattamentogas'] = 'Trattamento gas';
$lang['page_service_energia'] = 'Energia';
$lang['page_service_energia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_centralielettriche'] = 'Centrali elettriche';
$lang['page_service_centralinucleari'] = 'Centrali nucleari';
$lang['page_service_sottostazioni'] = 'Sottostazioni';
$lang['page_service_eolico'] = 'Eolico';
$lang['page_service_fotovoltaico'] = 'Fotovoltaico';
$lang['page_service_siderurgia'] = 'Siderurgia';
$lang['page_service_siderurgia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_acciaieria'] = 'Acciaieria';
$lang['page_service_primario'] = 'Primario';
$lang['page_service_laminazione'] = 'Laminazione';
$lang['page_service_tubificio'] = 'Tubificio';
$lang['page_service_navale'] = 'Navale';
$lang['page_service_navale_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_componenti'] = 'Componenti';
$lang['page_service_chimico'] = 'Chimico';
$lang['page_service_chimico_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_ammoniaurea'] = 'Ammonia-urea';
$lang['page_service_polimeri'] = 'Polimeri';
$lang['page_service_impiantochimico'] = 'Impianto chimico';
$lang['page_service_ecologia'] = 'Ecologia';
$lang['page_service_ecologia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_trattamentoacqua'] = 'Trattamento acqua';
$lang['page_service_dissalatori'] = 'Dissalatori';
$lang['page_service_trattamentorifiuti'] = 'Trattamento rifiuti';
$lang['page_service_sollevamento'] = 'Sollevamento';
$lang['page_service_sollevamento_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_grubraccio'] = 'Gru a braccio';
$lang['page_service_grucavalletto'] = 'Gru a cavalletto';
$lang['page_service_scaricatoricontainer'] = 'Scaricatori container';
$lang['page_service_grubanchina'] = 'Gru a banchina';
$lang['page_service_ferratigommati'] = 'Ferrati e Gommati';
$lang['page_service_carriponte'] = 'Carriponte';
$lang['page_service_sector_petrolifero'] = 'Settore Petrolifero';
$lang['page_service_sector_siderurgico'] = 'Settore Siderurgico';
$lang['page_service_sector_energia'] = 'Settore Energia';
$lang['page_service_sector_navale'] = 'Settore Navale';
$lang['page_service_sector_chimico'] = 'Settore Chimico';
$lang['page_service_sector_ecologia'] = 'Settore Ecologia';
$lang['page_service_sector_sollevamento'] = 'Settore Mezzi di Sollevamento';
$lang['page_service_sector_altri'] = 'Altre Attivita';


/****************WORK**********************************/
$lang['page_work_intro'] = '<p>TECHNOSERVICE S.r.l. accoglie qualsiasi candidatura e garantisce pari opportunità&nbsp; ai candidati.
La candidatura è da porre sotto forma di Curriculum Vitae con l\' autorizzazione al trattamento dati.</p>
<p>Al di là delle competenze personali ed alle qualifiche tecniche, sarà data priorità ai candidati con un
buona conoscenza  delle normative e degli standard nazionali ed internazionali applicabili, ed una buona conoscenza 
della lingua inglese e del personal computer (sistema operativo Windows).</p>';
$lang['page_work_subtitle'] = 'Posizioni di lavoro attualmente ricercate';
$lang['page_work_list'] = '<li>WELDING INSPECTOR (ITALIA / ESTERO)</li>
<li>ISPETTORE MECCANICO II LIVELLO – 4 METODI (Rif. ASNT TC 1A) UT, RT, MT, PT  (ITALIA / ESTERO)</li>
<li>SUPERVISORE QA/QC MECCANICO   </li><li>SUPERVISORE QA/QC ELETTRICO </li><li>PROJECT MANAGER </li>
<li>RESPONSABILE TECNICO DI CANTIERE  </li><li>SUPERVISORE MECCANICO DI CANTIERE </li><li>SUPERVISORE FLUIDISTICA DI CANTIERE  </li>
<li>SUPERVISORE ELETTRO-STRUMENTALE DI CANTIERE </li><li>RESPONSABILE COMMISSIONING DI CANTIERE  </li>';


/****************CONTACTS**********************************/
$lang['page_contacts_network_ispettori'] = 'NETWORK TECHNO SERVICE NAZIONI ESTERE - ISPETTORI LOCALI';
$lang['page_contacts_societa_estere'] = 'SOCIETA\' AFFILIATE ALL\' ESTERO';
$lang['page_contacts_filiali_estere'] = 'FILIALI ESTERE';
$lang['page_contacts_sedi_operativi'] = 'SEDI OPERATIVE';
$lang['page_contacts_sede_principale'] = 'SEDE PRINCIPALE - GENOVA';

/****************FOOTER**********************************/
$lang['page_footer_contacts_intro_title'] = 'Techno Service S.r.l.';
$lang['page_footer_contacts_intro'] = '<p>Servizi Tecnici Generali</p>';
$lang['page_footer_contacts_works_tag'] = '<p><b>INDAGINI</b> - ISPEZIONI - <b>EXPEDITING</b> - ASSICURAZIONE QUALITÀ - <b>CONTROLLO QUALITÀ</b></p>';
$lang['page_footer_contacts_main_office_title'] = '<h3>Ufficio Principale</h3>';
$lang['page_footer_contacts_main_office'] = '<p>Via Pietro Chiesa 9<br>16149 Genova - Italia</p>
<p>Tel. +39.010.4691301<br>Fax +39.010.4691366 </p>
<p>C.C.I.A.A. : 284212<br>P.IVA : 02593920107</p>';
$lang['page_footer_contacts_registered_office_title'] = '<h3>Ufficio USA</h3> ';
$lang['page_footer_contacts_registered_office'] = '<p>Delaware <br>c/o National Registered Agents</p>
<p>Inc. 160 Greentree Drive, Suite 101 Dover</p>
<p>DE 19904 County of Kent - USA</p>';
$lang['page_footer_menu_header'] = 'Links Utili';
$lang['page_footer_other_office'] = 'I Nostri uffici nel Mondo';
$lang['page_footer_other_office_title'] = 'I Nostri uffici nel Mondo';
