<?php

/****************HOMEPAGE**********************************/
$lang['page_home_tagline'] = 'Since 1984 We carry out our global services on behalf of leading companies, in Italy and in the world';
$lang['page_home_intro_title'] = '<span class="text-rosso">Quality Control</span>- Survey - 
<span class="text-rosso">Inspection Testing</span> - Expediting';
$lang['page_home_intro_text'] = 'Since 2010 TECHNO SERVICE has been certified by the Istituto Italiano della Saldatura (I.I.S.), 
a primary Italian Certification Body in accordance with UNI EN ISO 9001:2015 requirements;  n° certificate 355 (enclosed).';
$lang['page_home_quality_title'] = 'Quality at the customer\'s service';
$lang['page_home_quality_text'] = '<p>TECHNO SERVICE is being undertaking for years a path aiming to the continuous 
improvement of its internal process and to the satisfaction of client requirements.</p><p>In order to get a steady development, 
TECHNO SERVICE gives the maximum importance to the continuous implementation and improvement of its quality management system</p>';
$lang['page_home_services_title'] = 'Our Services';
$lang['page_home_services_text'] = '<p>TECHNO SERVICE offers tecnical assistance specialized service, audits, surveys, 
in order to ensure the quality, the compliance of system, processes and management systems.</p><p>Thanks to its problem 
solving competences, decision-making skills, professionalism, TECHNO SERVICE assists clients 
in reaching their goals by trained staff able to offer customer support at various stages of development of each project, 
the delivery of services and technical supervision.</p>';
$lang['page_home_map_title'] = 'TecnoService in the world';
$lang['page_home_cta_title_whatwedo'] = '';
$lang['page_home_cta_text_whatwedo'] = '<p><span class="font-weight-bold">Since 1984 TechnoService work at its customers\' service . 
<br>We </span><span class="font-italic">constantly</span> <span class="font-weight-bold">monitor our structure to </span>
<span class="font-italic">guarantee</span> <span class="font-weight-bold">this quality</span></p>';
$lang['page_home_cta_button_text_whatwedo'] = 'What We Do';
$lang['page_home_cta_button_url_title_whatwedo'] = '';
$lang['page_home_cta_title_workwithus'] = '';
$lang['page_home_cta_text_workwithus'] = '<p>We are proud of the work we do and we would not succeed without our fantastic team of dedicated people. Want to join our team?</p>';
$lang['page_home_cta_button_text_workwithus'] = 'Work with us';
$lang['page_home_cta_button_url_title_workwithus'] = '';


/****************ABOUT**********************************/
$lang['page_about_intro_text'] = 'TECHNO SERVICE carries out its following activities employing technicians with experience 
in the industrial sectors:<br> Mechanical - Electrical - Instrumentation - Automation - Civil - Refractory.';
$lang['page_about_timeline_title_one'] = 'Techno Service, a reality since 1984';
$lang['page_about_timeline_body_one'] = '<p>Since 1984 TECHNO SERVICE carries out services of:</p>
<ul class=""><li class="">Quality control</li><li class="">Survey</li><li class="">Inspection</li>
<li class="">Testing</li><li class="">Expediting</li></ul><p>These services are performed by Techno Service on behalf 
of primary EPC contractors operating in Italy and worldwide..</p>';
$lang['page_about_timeline_date_one'] = 'Since 1984';
$lang['page_about_timeline_title_two'] = 'Supervision services';
$lang['page_about_timeline_body_two'] = '<p>TECHNO SERVICE since 1990 has developed and improved his core business 
worldwide in field related to service supervision on site as:</p>
<ul class=""><li class="">QA/QC Management</li><li class="">Quality Site Supervision</li>
<li class="">Erection Assistance</li><li class="">Pre-Commissioning</li>
<li class="">Start Up</li><li class="">Tuning</li></ul>';
$lang['page_about_timeline_date_two'] = 'Since 1990';
$lang['page_about_timeline_title_three'] = 'UNI EN ISO 9001:2015 Certification';
$lang['page_about_timeline_body_three'] = '<p>Since 2010 TECHNO SERVICE has been certified by the Istituto Italiano 
della Saldatura (I.I.S.), a primary Italian Certification Body in accordance with UNI EN ISO 9001:2015 requirements; 
<a href="/documents/Certificato_355_UNI_EN_ISO_9001_2015.pdf" target="_new">n° certificate 355 (enclosed)</a>.</p>';
$lang['page_about_timeline_date_three'] = 'Since 2010';
$lang['page_about_tecnici_qualificati_title'] = 'Qualified Technicians';
$lang['page_about_tecnici_qualificati_body'] = '<p>Our Technicians dealing with Non Destructive Controls Procedures 
have the II° and III° level, according to the national standards CIC PND and the international standards ASNT TC 1A for:</p>
<ul class=""><li class="">VT (Visual Method)</li><li class="">UT (Ultrasonic Method)</li><li class="">PT (Penetrating Method)</li>
<li class="">RT (Radiographic Method)</li><li class="">MT (Magnetoscopic Method)</li></ul>
<p>Qualified welding inspectors operate in compliance with applicable national and international standards (IIW qualified).</p>
<p>Technical staff include also ARAMCO qualified inspector booth for vendor\'s inspection activities and fields activities.</p>
<p>We provide for all our employes and for our technicians a group accident policies issued by primary international assurance companies.</p>';
$lang['page_about_politica_qualita_title'] = 'Techno Service - Quality Policy';
$lang['page_about_politica_qualita_body'] = '<p>TECHNO SERVICE is being undertaking for years a path aiming to the continuous 
improvement of its internal process and to the satisfaction of client requirements.</p>
<p>Company\'s organization and compliance program, in accordance to UNI EN ISO 9001 standards, has permitted to obtain
	<a href="/documents/Certificato_355_UNI_EN_ISO_9001_2015.pdf" target="_new"> UNI EN 9001:2015 certification</a>
	from the Istituto Italiano Della Saldatura.</p>
<p>In order to get a steady development, TECHNO SERVICE gives the maximum importance to the continuous implementation 
and improvement of its quality management system with the aim to satisfy the following quality steps:</p>
<ul><li>organization client oriented</li><li>leadership </li><li>personnel involvement</li>
<li>continuous improvement</li><li>decision making based on solid and reliable elements</li></ul>
<p>Our company gives also the maximum importance to the conformance, development and application by all our personnel 
to quality system management criteria referred to uni en iso 9001:2015 and to the achievement of the goals, providing 
the full satisfaction of the client.</p>
<p>For this purpose it is requested and monitored the constant support and a continuous involvement of 
all Techno Service structure.</p>';


/****************SERVICES**********************************/
$lang['page_service_intro'] = '<p>TECHNO SERVICE offers tecnical assistance specialized service, audits, surveys, 
in order to ensure the quality, the compliance of system, processes and management systems.
</p><p>Thanks to its problem solving competences, decision-making skills, professionalism, TECHNO SERVICE assists clients in 
reaching their goals by trained staff able to offer customer support at various stages of development of each project, 
the delivery of services and technical supervision.</p>';

$lang['page_service_oil_gas'] = 'Oil And Gas';
$lang['page_service_oil_gas_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_onoffshore'] = 'On/Offshore plant';
$lang['page_service_piattaforma'] = 'Platform';
$lang['page_service_barge'] = 'Barge';
$lang['page_service_pipeline'] = 'Pipeline';
$lang['page_service_rivestimenti'] = 'Coating';
$lang['page_service_raffineria'] = 'Refinery';
$lang['page_service_trattamentogas'] = 'Gas treatment plant';
$lang['page_service_energia'] = 'Energy';
$lang['page_service_energia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_centralielettriche'] = 'Electric power plant';
$lang['page_service_centralinucleari'] = 'Nuclear power plant';
$lang['page_service_sottostazioni'] = 'Substation';
$lang['page_service_eolico'] = 'Eolic power plant';
$lang['page_service_fotovoltaico'] = 'Photovoltaic plant';
$lang['page_service_siderurgia'] = 'Siderurgical';
$lang['page_service_siderurgia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_acciaieria'] = 'Iron and steeel industry';
$lang['page_service_primario'] = 'Primary';
$lang['page_service_laminazione'] = 'Lamination';
$lang['page_service_tubificio'] = 'Tube mills';
$lang['page_service_navale'] = 'Marine';
$lang['page_service_navale_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_componenti '] = 'Marine equipments';
$lang['page_service_chimico'] = 'Chemical';
$lang['page_service_chimico_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_ammoniaurea'] = 'Ammonia-urea plant';
$lang['page_service_polimeri '] = 'Polymers';
$lang['page_service_impiantochimico'] = 'Chemical plant';
$lang['page_service_ecologia'] = 'Ecology';
$lang['page_service_ecologia_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_trattamentoacqua'] = 'Water treatment plant';
$lang['page_service_dissalatori'] = 'Desalination plant';
$lang['page_service_trattamentorifiuti'] = 'Waste treatment plant';
$lang['page_service_sollevamento'] = 'Hoisting equipments';
$lang['page_service_sollevamento_descr'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
$lang['page_service_grubraccio'] = 'Jib crane';
$lang['page_service_grucavalletto'] = '';
$lang['page_service_scaricatoricontainer'] = 'Transtainer';
$lang['page_service_grubanchina'] = 'Gantry crane';
$lang['page_service_ferratigommati'] = 'Portainer';
$lang['page_service_carriponte'] = 'Bridge crane';
$lang['page_service_sector_petrolifero'] = 'Oil and Gas Trade';
$lang['page_service_sector_siderurgico'] = 'Siderurgical Trade';
$lang['page_service_sector_energia'] = 'Energy Trade';
$lang['page_service_sector_navale'] = 'Marine Trade';
$lang['page_service_sector_chimico'] = 'Chemical Trade';
$lang['page_service_sector_ecologia'] = 'Ecology Trade';
$lang['page_service_sector_sollevamento'] = 'Lifting Equipment Trade';
$lang['page_service_sector_altri'] = 'Others';

/****************WORK**********************************/
$lang['page_work_intro'] = '<p>TECHNO SERVICE SRL will welcome each candidate application and can grant equal opportunities 
to all applicants, who are required to send their curriculum-vitae indicating their authorization to use and process 
personal details.</p>
<p>Over and above the personal competences and technical qualifications, it will be given priority to the candidates having
a good knowledge of national and international applicable codes and standards and a good knowledge of english language 
and personal computer (Windows operating system).</p>';
$lang['page_work_subtitle'] = 'Positions currently required';
$lang['page_work_list'] = '<li>WELDING INSPECTOR (ITALY and ABROAD)</li>
<li>MECHANICAL INSPECTOR II LEVEL - 4 METHODS (REF ASNT TC 1A) UT, RT, MT, PT (ITALY AND ABROAD)</li>
<li>QA / QC FIELD SUPERVISOR - MECHANICAL</li><li>QA / QC FIELD SUPERVISOR - ELECTRICAL</li><li>PROJECT MANAGER</li>
<li>SITE TEHNICAL RESPONSIBLE</li><li>SITE MECHANICAL SUPERVISOR</li><li>SITE FLUID SUPERVISOR</li>
<li>SITE ELECTRICAL / INSTRUMENTATION SUPERVISOR</li><li>SITE COMMISSIONING RESPONSIBLE</li>';

/****************CONTACTS**********************************/
$lang['page_contacts_network_ispettori'] = 'TECHNO SERVICE MAIN COUNTRIES OPERATING WITH LOCAL INSPECTORS NETWORK';
$lang['page_contacts_societa_estere'] = 'TECHNO SERVICE AFFILIATED COMPANY';
$lang['page_contacts_filiali_estere'] = 'TECHNO SERVICE BRANCHES';
$lang['page_contacts_sedi_operativi'] = 'OPERATIONS OFFICE';
$lang['page_contacts_sede_principale'] = 'ITALY - HEAD OFFICE';


/****************FOOTER**********************************/
$lang['page_footer_contacts_intro_title'] = 'Techno Service S.r.l.';
$lang['page_footer_contacts_intro'] = '<p>General Technical Services</p>';
$lang['page_footer_contacts_works_tag'] = '<p><b>SURVEY</b> - INSPECTIONS - <b>EXPEDITING</b> - QUALITY ASSURANCE - <b>QUALITY CONTROL</b></p>';
$lang['page_footer_contacts_main_office_title'] = '<h3>Main Office</h3>';
$lang['page_footer_contacts_main_office'] = '<p>Via Pietro Chiesa 9<br>16149 Genova - Italia</p>
<p>Tel. +39.010.4691301<br>Fax +39.010.4691366 </p>
<p>C.C.I.A.A. : 284212<br>P.IVA : 02593920107</p>';
$lang['page_footer_contacts_registered_office_title'] = '<h3>USA Office</h3>';
$lang['page_footer_contacts_registered_office'] = '<p>Delaware c/o National Registered Agents</p>
<p>Inc. 160 Greentree Drive, Suite 101 Dover</p>
<p>DE 19904 County of Kent - USA </p>';
$lang['page_footer_menu_header'] = 'Useful Links';
$lang['page_footer_other_office'] = 'Our Office All around the world';
$lang['page_footer_other_office_title'] = 'Office All around the world';
