<?php

/**
 * Created by IntelliJ IDEA.
 * User: Cangellini
 * Date: 03/02/2019
 * Time: 19:16
 */
class LanguageLoader
{
	public function initialize() {
		$ci =& get_instance();
		$ci->load->helper('language');
		$ci->lang->load('pages','italian');

		$siteLang = $ci->session->userdata('site_lang');

		if ($siteLang) {
			$ci->lang->load('pages',$siteLang);
		} else {
			$ci->lang->load('pages','italian');
		}

		/*$site_lang = $ci->session->userdata('site_lang');
		if ($site_lang) {
			$ci->lang->load('pages',$ci->session->userdata('site_lang'));
		} else {
			$ci->lang->load('pages','italian');
		}*/
	}
}
