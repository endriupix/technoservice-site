<header>
	<div class="jumbotron text-center">
		<div>
			<h1 class="display-4 font-weight-bold text-uppercase"><?php echo PROPRIETARIO; ?></h1>
			<p class="lead"><?php echo $this->lang->line('page_home_tagline'); ?></p>
			<a class="btn btn-primary btn-lg" href="<?php echo $this->lang->line('route_about'); ?>" role="button"><?php echo $this->lang->line('string_about'); ?></a>
		</div>

	</div>

</header>
<main>
<div id="intro-container-section">
	<div class="container-fluid bkg-nero text-bianco">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2 class=""><?php echo $this->lang->line('page_home_intro_title'); ?></h2>
				</div>
				<div class="col-md-6">
					<p><?php echo $this->lang->line('page_home_intro_text'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="about-container-section">
	<div class="container-fluid bkg-bianco">
		<!-- Features Section -->
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h2><?php echo $this->lang->line('page_home_quality_title'); ?></h2>
						<hr class="hr-default">
						<?php echo $this->lang->line('page_home_quality_text'); ?>
					</div>
					<div class="col-lg-6">
						<img class="img-fluid rounded" src="/img/stock-photo-female-site-engineer-with-white-hardhat-and-safety-jacket-and-silhouette-construction-background-740015797.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
</div>

<?php

//imposto x a zero per stampare la prima cta dell'array
$index['x'] = 0;
$this->load->view('modules/call_to_action',$index); ?>
<div id="services-container-section">
	<div class="container-fluid bkg-grigio">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col text-left">
						<h2><?php echo $this->lang->line('page_home_services_title'); ?></h2>
						<hr class="hr-default">
						<p><?php echo $this->lang->line('page_home_services_text'); ?></p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row"><?php $this->load->view('modules/services_grid_title'); ?></div>
			</div>
		</div>
	</div>
</div>


<?php
$index['x'] = 1;
$this->load->view('modules/call_to_action',$index); ?>

<div id="customers-container-section" class="">
	<div class="container">
		<?php $this->load->view('modules/carousel_logos'); ?>
	</div>
</div>

<div id="map-title-container-section" class="text-center mb-4">
<h2><?php echo $this->lang->line('page_home_map_title'); ?></h2>
<hr class="hr-default-center">
</div>
<?php $this->load->view('modules/map_all_width'); ?>

