<?php echo $this->breadcrumb->output(); ?>

<section id="office-list-section" class="container">
	<div class="row">
		<div class="col-sm-12"><h2 class="mb-4"><?php echo $this->lang->line('page_contacts_sede_principale'); ?></h2></div>
		<div class="col-md-8">
<!--			<h2 class="mb-4">--><?php //echo $this->lang->line('page_contacts_sede_principale'); ?><!--</h2>-->

			<p><b>TECHNO SERVICE S.R.L.</b></p>

			<p><i class="fas fa-map-marker-alt"></i> Torri Piane Centro Direzionale di San Benigno - Via Pietro Chiesa 9 - 16149, Genova - Italia</p>
			<p><i class="fas fa-phone"></i> +39.010.4691301&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<i class="fas fa-fax"></i> +39.010.4691366</p>
			<p>
				<i class="fas fa-envelope"></i><a href="mailto:info@technoservicege.com"> info@technoservicege.com</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="skype:technoservicege?call"><i class="fab fa-skype"></i>  Contattaci su Skype</a>
			</p>
			<p><i class="fas fa-tag"></i> C.C.I.A.A. : 284212&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;P. IVA : 02593920107</p>

			<hr class="hr-default mt-4 mb-4">

			<p>
				<b><?php echo $this->lang->line('string_direzione'); ?></b>
			</p>
			<p class="ml-4">
				<i class="fas fa-user"></i> Mario Cerruti&nbsp;&nbsp;&nbsp;
				<i class="fas fa-envelope"></i><a href="mailto:mario.cerruti@technoservicege.com"> mario.cerruti@technoservicege.com</a>
			</p>
			<p class="ml-4">
				<i class="fas fa-user"></i> Clemente Bosi&nbsp;&nbsp;&nbsp;
				<i class="fas fa-envelope"></i><a href="mailto:clemente.bosi@technoservicege.com"> clemente.bosi@technoservicege.com</a>
			</p>
			<p>
				<b><?php echo $this->lang->line('string_amministrazione'); ?></b>
			</p>
			<p class="ml-4">
				<i class="fas fa-user"></i> Luisa Arrigo&nbsp;&nbsp;&nbsp;
				<i class="fas fa-envelope"></i><a href="mailto:luisa.arrigo@technoservicege.com"> luisa.arrigo@technoservicege.com</a>
			</p>

		</div>
		<div class="col-md-4">
			<img src="/img/stock-photo-the-freighter-docked-at-the-dock-for-loading-and-unloading-698194447.jpg" class="img-fluid" alt="">
		</div>

	</div>
	<hr class="hr-default-center mt-4 mb-4">
	<div class="row">
		<div class="col-sm-12 mt-4">

			<h2><?php echo $this->lang->line('page_contacts_sedi_operativi'); ?></h2>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE S.r.l. Cagliari (Italy)</b><br>
				Via Ischia 4<br>
				09012 - Capoterra Cagliari - ITALY<br>
				Tel. +39 070 710810 <br>
				Fax. +39 070 7109170 <br>
			</p>

		</div>
		<div class="col-lg-6">
			<p>

				<b>TECHNO SERVICE LLC USA c/o Burns &amp; Levinson LLP</b><br>
				125 Summer Street, Boston MA 02110 - USA<br>
				Ph.   + 1 617 345 3627<br>
				Fax: + 1 617 345 3299<br>
			</p>

			<p>
				Registered Office: Technoservice LLC c/o National Registered Agents, Inc. 160 Greentree Drive, Suite 101, Dover, DE 19904 County of Kent - USA
			</p>
		</div>


	</div>
	<hr class="hr-default-center mt-4 mb-4">
<div class="row">
		<div class="col-sm-12 mt-4">
			<h2><?php echo $this->lang->line('page_contacts_filiali_estere'); ?></h2>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE INDIA
					c/o INTERNATIONAL INSPECTION SERVICES (P) LTD</b>
				<br>Plot No. 29, # 5-6-137/6/4/2, Behind Metro,
				<br>Sri Sai Nagar Colony, Near Sai Baba Temple,
				<br>Kukatpally, Hyderabad - 500 072 INDIA
				<br>Ph. +91 040 6455 3403
				<br>E-mail: iispl@eth.net
				<br>Web: www.iispl.net
			</p>

		</div>
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE CHINA
					c/o THARSOS</b>
				<br>2701#, The South Tower, Huaninginternational plaza,
				<br>No. 300 Xuanhua Road, Changing Diostrict,
				<br>Shanghai, CHINA
			</p>
		</div>


	</div>
	<hr class="hr-default-center mt-4 mb-4">

<div class="row">
		<div class="col-sm-12 mt-4">
			<h2><?php echo $this->lang->line('page_contacts_societa_estere'); ?></h2>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE U.K.</b>
				<br>c/o NEIL BARNETT INSPECTION SERVICES LTD
				<br>10, Bordeaux Close, Northfield Green, SR3 2SR
				<br>Sunderland - U.K.

			</p>

		</div>
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE Poland / Ukraina</b>
				<br>c/o AdvaServe
				<br>ul. Lanciego, 16/137
				<br>02-792 Warszawa - Poland
				<br>Ph. + 48 22 859 16 78
				<br>Fax + 48 22 859 16 83

			</p>
		</div>
	</div>
<div class="row">
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE Romania / Czech Rep.</b>
				<br>c/o S.C. MIGRAL SRL
				<br>Novai 5, Sector 5
				<br>051725- Bucharest - ROMANIA
				<br>Ph. +40 0722343041
				<br>E-mail: demi.perparim@yahoo.com

			</p>

		</div>
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE Tunisie / Algeria / Libya / Morocco</b>
				<br>c/o B A M Audit &amp; Conseil SARL
				<br>9 Rue de Jérusalem, 1002, Tunis - Tunisie
				<br>Ph. + 216 71 788 378 / +216 71 791 954
				<br>Fax + 216 71 791 954

			</p>
		</div>
	</div>
<div class="row">
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE Saudi Arabia</b>
				<br>c/o Mr. S. TAMIZHSELVAN,
				<br>Afaal Al Khaleej Est,
				<br>P.O. Box: 840 - Ras Tanura 31941
				<br>KINGDOM OF SAUDI ARABIA

			</p>

		</div>
		<div class="col-lg-6">
			<p>
				<b>TECHNO SERVICE SINGAPORE</b>
				<br>c/o South East Asia Operations
				<br>Singapore Branch Office
				<br>Blk 473, Choa Chukang
				<br>Avenue 3, # 13-165,
				<br>SINGAPORE - 680473

			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<p>
				<b>TECHNO SERVICE U.A.E.</b>
				<br>c/o Mr. IMRAN
				<br>P.O. Box: 2484
				<br>Ajman - U.A.E.
			</p>

		</div>
	</div>
	</section>

<section id="map-section" class="">
	<div class="container-fluid">
		<div id='map-home'></div>
	</div>
</section>
<section id="network-section" class="container">
	<div class="row">
		<div class="col-sm-12 mb-4 text-center">
			<h2><?php echo $this->lang->line('page_contacts_network_ispettori'); ?></h2>
			<hr class="hr-default-center">
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<ul>
				<li>ARGENTINA                                  </li>
				<li>AUSTRIA                                    </li>
				<li>BRAZIL                                     </li>
				<li>BULGARIA                                   </li>
				<li>BELGIUM                                    </li>
				<li>CROATIA                                    </li>
				<li>DENMARK                                    </li>
				<li>EGYPT                                      </li>
				<li>ESTONIA/LITUANIA                           </li>
				<li>FINLAND                                    </li>
				<li>GERMANY                                    </li>
				<li>GREECE/CYPRUS                              </li>
				<li>HOLAND                                     </li>
				<li>HUNGARY                                    </li>
				<li>IRELAND                                    </li>
				<li>JAPAN                                      </li>
				<li>KAZHAKSTAN                                 </li>
				<li>KOREA                                      </li>
				<li>NORWAY                                     </li>
				<li>RUSSIA                                     </li>
				<li>SENEGAL                                    </li>
				<li>SLOVAKIA                                   </li>
				<li>SLOVENIA                                   </li>
				<li>SOUTH AFRICA                               </li>
				<li>SPAIN/PORTUGAL                             </li>
				<li>SWEDEN                                     </li>
				<li>SWITZERLAND / LUXEMBOURG / P.MONACO            </li>
				<li>TURKEY                                     </li>
			</ul>
		</div>
		<div class="col-md-6">
			<img src="/img/stock-photo-oil-drilling-rig-in-sunset-time-at-sea-248911237.jpg" alt="" style="    width: 100%;">
		</div>
	</div>
</section>



