<?php echo $this->breadcrumb->output(); ?>

<section id="work-with-us-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<?php echo $this->lang->line('page_work_intro'); ?>

				<h2 class="mt-5"><?php echo $this->lang->line('page_work_subtitle'); ?></h2>
				<hr class="hr-default">

				<ul class="list mt-4">
					<?php echo $this->lang->line('page_work_list'); ?>
				</ul>

				<p>
					<?php echo $this->lang->line('string_inviare_candidatura'); ?>
					<a href="mailto:info@technoservicege.com"> info@technoservicege.com</a>
				</p>
			</div>
			<div class="col-lg-6">
				<img src="/img/stock-photo-four-businessmen-s-hands-collect-a-puzzle-of-gears-against-the-sunset-business-concept-idea-1106163755.jpg"
					 alt="" style="width: 100%">
			</div>
		</div>
	</div>
</section>
