<?php echo $this->breadcrumb->output(); ?>

<div id="clients-list-section">
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<article>
				<section>
					<h2><?php echo $this->lang->line('page_service_sector_petrolifero'); ?></h2>
					<ul>
						<li>AGIP PETROLI</li>
						<li>AGIP N.A.M.E.</li>
						<li>ELF PETROLEUM</li>
						<li>ENI</li>
						<li>ERG PETROLI</li>
						<li>GERMANISCHER LLOYD</li>
						<li>INTERTEK</li>
						<li>NATIONAL PETROLEUM CONTRUCTION COMPANY</li>
						<li>NOOTER ERIKSEN</li>
						<li>REGISTRO ITALIANO NAVALE</li>
						<li>SAIPEM</li>
						<li>SAIPEM ENERGY SERVICES</li>
						<li>SAIPEM PORTUGAL</li>
						<li>SAIPEM NIGERIA</li>
						<li>SAIPEM SAUDI</li>
						<li>SNAM</li>
						<li>SNAM PROGETTI</li>
						<li>SONSUB</li>
						<li>TECHINT</li>
					</ul>
				</section>
			</article>
			<hr class="hr-default mt-5 mb-5">
			<article>
				<section>
					<h2><?php echo $this->lang->line('page_service_sector_energia'); ?></h2>
					<ul>
						<li>AERIMPIANTI</li>
						<li>ANSALDO DIV. IMPIANTI</li>
						<li>ANSALDO ENERGIA / ANSALDO NUCLEARE</li>
						<li>BORSIG Process Heat</li>
						<li>CALENIA ENERGIA</li>
						<li>EGL ITALIA</li>
						<li>ELECKTROWATT EKONO</li>
						<li>ENEL Power</li>
						<li>ENI POWER</li>
						<li>GENERAL CONSTRUCTION</li>
						<li>JAKKO POYRY GROUP</li>
						<li>SIEMENS</li>
						<li>SNAMPROGETTI</li>
						<li>S.T.F.</li>
						<li>ENI POWER</li>
					</ul>
				</section>
			</article>

		</div>
		<div class="col-lg-6">
			<article>
				<section>
					<h2><?php echo $this->lang->line('page_service_sector_siderurgico'); ?></h2>
					<ul>
						<li>ABB SAE SADELMI</li>
						<li>ACCIAI SPECIALI TERNI KRUPP</li>
						<li>ANSALDO SISTEMI INDUSTRIALI</li>
						<li>DALMINE</li>
						<li>DALMINE ATB</li>
						<li>DANIELI</li>
						<li>DANIELI AUTOMATION</li>
						<li>DANIELI CENTRO COMBUSTION</li>
						<li>DAVY INTERNATIONAL</li>
						<li>HEURTEY ITALIANA</li>
						<li>ITALIMPIANTI</li>
						<li>ILVA NOVI</li>
						<li>ILVA TARANTO</li>
						<li>ILVA GENOVA</li>
						<li>ILVA PIOMBINO</li>
						<li>ILVA BAGNOLI</li>
						<li>ILVA AOSTA</li>
						<li>LUCCHINI SIDERURGIA</li>
						<li>PAUL WURTH</li>
						<li>RIVA</li>
						<li>SIDERMONTAGGI</li>
						<li>SMS DEMAG INNSE</li>
						<li>SOCIETA' DELLE FUCINE</li>
						<li>TAGLIAFERRI</li>
						<li>TECHINT</li>
						<li>TENOVA</li>
						<li>VOEST - ALPINE INDUSTRIEANLAGENBAU</li>
						<li>VOEST - ALPINE IMPIANTI</li>
					</ul>
				</section>
			</article>
			<hr class="hr-default mt-5 mb-5">
			<article>
				<section>
					<h2><?php echo $this->lang->line('page_service_sector_navale'); ?></h2>
					<ul>
						<li>FINCANTIERI</li>
						<li>SAIPEM</li>
					</ul>
				</section>
			</article>
		</div>


	</div>
</div>
	<div class="container-fluid mt-5 mb-5">
		<?php $this->load->view('modules/carousel_logos'); ?>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<article>
					<section>
						<h2><?php echo $this->lang->line('page_service_sector_ecologia'); ?></h2>
						<ul>
							<li>ABB SAE SADELMI</li>
							<li>ALSTOM POWER ITALIA</li>
							<li>CONSORZIO ANSCO CAIRO</li>
							<li>DANECO DANIELI ECOLOGIA</li>
							<li>DANECO-TECNIMONT ECOLOGIA</li>
							<li>FISIA ITALIMPIANTI</li>
							<li>GERMANISCHER LLOYD</li>
							<li>IMPREGILO</li>
							<li>RINA INDUSTRY</li>
						</ul>
					</section>
				</article>

			</div>
			<div class="col-lg-6">
				<article>
					<section>
						<h2><?php echo $this->lang->line('page_service_sector_chimico'); ?></h2>
						<ul>
							<li>FILIPPO FOCHI</li>
							<li>MOODY INT.</li>
							<li>SNAMPROGETTI</li>
							<li>TECNIMONT</li>
							<li>TECTUBI</li>
						</ul>
					</section>
				</article>
				<hr class="hr-default mt-5 mb-5">
				<article>
					<section>
						<h2><?php echo $this->lang->line('page_service_sector_sollevamento'); ?></h2>
						<ul>
							<li>FINCANTIERI</li>
							<li>NUOVA MAGRINI GALILEO</li>
							<li>VOLTRI TERMINAL EUROPA</li>
						</ul>
					</section>
				</article>
				<hr class="hr-default mt-5 mb-5">
				<article>
					<section>
						<h2><?php echo $this->lang->line('page_service_sector_altri'); ?></h2>
						<ul>
							<li>FINTECNA / CIMI MONTUBI</li>
						</ul>
					</section>
				</article>
			</div>


		</div>
	</div>
</div>


