<?php echo $this->breadcrumb->output(); ?>

<!-- Page Content -->
<!-- /.row -->

<div class="container timeline-title">
	<div class="row">
		<div class="col-sm-12 text-center">
			<h2>
				<?php echo $this->lang->line('string_realta_ts'); ?>
			</h2>
			<hr class="hr-default-center">

		</div>
	</div>

</div>
<section id="timeline-about-section" class="timeline mt-5">

	<div class="container">

		<div class="timeline-item">
			<div class="timeline-img"></div>

			<div class="timeline-content js--fadeInLeft">
				<div class="timeline-img-header">
					<h2><?php echo $this->lang->line('page_about_timeline_title_one'); ?></h2>
				</div>

				<div class="date"><?php echo $this->lang->line('page_about_timeline_date_one'); ?></div>
				<div class="timeline-content-body">
					<?php echo $this->lang->line('page_about_timeline_body_one'); ?>
				</div>

			</div>
		</div>

		<div class="timeline-item">

			<div class="timeline-img"></div>

			<div class="timeline-content timeline-card js--fadeInRight">
				<div class="timeline-img-header">
					<h2><?php echo $this->lang->line('page_about_timeline_title_two'); ?></h2>
				</div>
				<div class="date"><?php echo $this->lang->line('page_about_timeline_date_two'); ?></div>
				<div class="timeline-content-body">
					<?php echo $this->lang->line('page_about_timeline_body_two'); ?>

				</div>
			</div>

		</div>

		<div class="timeline-item">

			<div class="timeline-img"></div>

			<div class="timeline-content js--fadeInLeft">
				<div class="timeline-img-header">
					<h2><?php echo $this->lang->line('page_about_timeline_title_three'); ?></h2>
				</div>

				<div class="date"><?php echo $this->lang->line('page_about_timeline_date_three'); ?></div>
				<div class="timeline-content-body">
					<?php echo $this->lang->line('page_about_timeline_body_three'); ?>
				</div>

			</div>

		</div>


	</div>
</section>

<?php $this->load->view('modules/call_to_action'); ?>

<section id="intro-azienda-section" class="">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>
					<?php echo $this->lang->line('string_our_services'); ?>
				</h2>
				<hr class="hr-default-center">
				<p class="description">
					<?php echo $this->lang->line('page_about_intro_text'); ?>
				</p>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-md-3 col-sm-6">
				<div class="serviceBox orange">
					<div class="service-icon">
						<i class="fas fa-search"></i>
					</div>
					<h3 class="title"><?php echo $this->lang->line('string_service_ispezione'); ?></h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="serviceBox orange">
					<div class="service-icon">
						<i class="fas fa-check-double"></i>
					</div>
					<h3 class="title"><?php echo $this->lang->line('string_service_collaudo'); ?></h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="serviceBox orange">
					<div class="service-icon">
						<i class="fas fa-poll"></i>
					</div>
					<h3 class="title"><?php echo $this->lang->line('string_service_survey'); ?></h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="serviceBox orange">
					<div class="service-icon">
						<i class="far fa-clock"></i>
					</div>
					<h3 class="title"><?php echo $this->lang->line('string_service_expediting'); ?></h3>
<?php /* <!--					<p class="description">-->
<!--						Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi, dolore quisquam illum.-->
<!--					</p>--> */ ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center mt-4">
				<p class="description">
					<a class="btn btn-primary" href="http://"><?php echo $this->lang->line('string_service_button_other'); ?></a>
				</p>
			</div>
		</div>
	</div>

</section>







<section id="tecnici-techno-service-section" class="container-fluid bkg-grigio-dark text-bianco">
	<div class="container">

		<!-- Intro Content -->
		<div class="row">

			<div class="col-lg-4  col-sm-push-8">
				<img class="img-fluid rounded mb-4" src="/img/stock-photo-foundry-worker-melt-steel-workpiece-and-controls-quality-1105674698.jpg" alt="">


			</div>
			<div class="col-lg-8  col-sm-pull-4">
				<h2><?php echo $this->lang->line('page_about_tecnici_qualificati_title'); ?></h2>
				<hr class="hr-default">
				<?php echo $this->lang->line('page_about_tecnici_qualificati_body'); ?>
			</div>
		</div>
	</div>
</section>
<!-- /.row -->

<section id="customers-container-section" class="">
	<div class="container">
		<div class="row mb-4">
			<div class="col-sm-12 text-center">

				<h2><?php echo $this->lang->line('string_our_clients'); ?></h2>
				<hr class="hr-default-center">
			</div>
		</div>
	</div>
	<div class="container-fluid">
	<?php $this->load->view('modules/carousel_logos'); ?>
	</div>
</section>

<section id="services-quality-section" class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="content" class="content text-center"> <!-- // START content -->

					<h2><?php echo $this->lang->line('page_about_politica_qualita_title'); ?></h2>
					<hr class="hr-default-center">
					<!-- Intro Content -->
					<div class="row">
						<div class="col-lg-7  col-sm-pull-5 text-left">


							<?php echo $this->lang->line('page_about_politica_qualita_body'); ?>

						</div>
						<div class="col-lg-5  col-sm-push-7">
							<img class="img-fluid rounded mb-4" src="/img/stock-photo-businessman-in-white-hard-hat-with-layout-isolated-on-white-165560810.jpg" alt="">


						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
