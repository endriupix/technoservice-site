<?php echo $this->breadcrumb->output() ?><!-- Page Heading/Breadcrumbs -->
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="index.php">Home</a>
	</li>
	<li class="breadcrumb-item active">About</li>
</ol>
<p><?php echo $this->lang->line('welcome_message'); ?></p>
<!-- Page Content -->

<!-- /.row -->
<section id="intro-azienda-section" class="">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">

				<p>
					TECHNO SERVICE è leader in attività di ispezione, collaudo, survey ed expediting impiegando tecnici di comprovata esperienza nei seguenti settori industriali:
				</p>
				<p>
					meccanico - elettrico - strumentazione - automazione - civile - refrattario
				</p>
			</div>
		</div>
	</div>

</section>
<section id="timeline-about-section" class="timeline">
	<div class="container">
		<div class="timeline-item">
			<div class="timeline-img"></div>

			<div class="timeline-content js--fadeInLeft">
				<div class="timeline-img-header">
					<h2>Techno Service, una realtà dal 1984</h2>
				</div>

				<div class="date">Dal 1984</div>
				<div class="timeline-content-body">
					<p>Sin dal 1984 TECHNO SERVICE svolge servizi di:</p>

					<ul class="">
						<li class="">Quality control</li>
						<li class="">Survey</li>
						<li class="">Inspection</li>
						<li class="">Testing</li>
						<li class="">Expediting</li>
					</ul>

					<p>
						Questi servizi sono svolti da Techno Service a livello mondiale
						per conto di primarie aziende operanti in Italia e nel Mondo.
					</p>
				</div>

			</div>
		</div>

		<div class="timeline-item">

			<div class="timeline-img"></div>

			<div class="timeline-content timeline-card js--fadeInRight">
				<div class="timeline-img-header">
					<h2>Card Title</h2>
				</div>
				<div class="date">Dal 1990</div>
				<div class="timeline-content-body">
					<p>
						TECHNO SERVICE sin dal 1990 ha sviluppato ed ampliato il proprio core business a livello internazionale, nel
						settore dei servizi di supervisore in cantiere quali:
					</p>
					<ul class="">
						<li class="">QA/QC Management</li>
						<li class="">Supervisione della Qualità in cantiere</li>
						<li class="">Assistenza al Montaggio</li>
						<li class="">Pre-Commissioning</li>
						<li class="">Avviamento</li>
						<li class="">Messa in Servizio</li>
					</ul>
					<p>
						I servizi di supervisione sono stati potenziati da Techno Service sia nel settore meccanico che in quello
						elettrico, automazione, civile, refrattari, parti di impianto ed impianti.
					</p>
					<a class="bnt-more" href="activities.html">Scopri tutti i servizi Techno Service...</a>
				</div>
			</div>

		</div>

		<div class="timeline-item">

			<div class="timeline-img"></div>

			<div class="timeline-content js--fadeInLeft">
				<div class="timeline-img-header">
					<h2>Certificazione UNI EN ISO 9001:2015</h2>
				</div>

				<div class="date">Dal 2010</div>
				<div class="timeline-content-body">
					<p>
						Dal 2010 TECHNO SERVICE è stata certificata dall' Istituto Italiano della Saldatura
						(I.I.S.), primario organismo di certificazione, in conformit&agrave; ai requisiti UNI EN ISO 9001:2015&nbsp;
						<a href="../../doc/Certificato_355.pdf" target="_new">(Certificato No. 355 allegato)</a>.
					</p>
				</div>

			</div>

		</div>


	</div>
</section>



<section id="customers-container-section" class="">
	<div class="container">
		<div class="row">
			<div class="col text-center">

				<h1>I Nostri Servizi</h1>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="customer-logos">
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
			<div class="slide"><img src="https://via.placeholder.com/80"></div>
		</div>
	</div>

</section>

<section id="tecnici-techno-service-section" class="container-fluid bkg-grigio-dark text-bianco">
	<div class="container">

		<!-- Intro Content -->
		<div class="row">

			<div class="col-lg-4">
				<img class="img-fluid rounded mb-4" src="/img/tecnici-specializzati-operai-techno-service-genova.jpg" alt="">


			</div>
			<div class="col-lg-8">
				<h2>Tecnici Qualificati</h2>

				<p>I nostri Tecnici, che si occupano delle procedure dei Controlli Non Distruttivi, sono in possesso
					dei LIVELLI II e III in conformità agli standard  CIC PND nazionali
					ed a quelli internazionali ASNT TC 1A per i metodi:
				</p>

				<ul class="">
					<li class="">VT  (Visivo)</li>
					<li class="">UT (Ultrasonoro)</li>
					<li class="">PT (Liquidi Penetranti)</li>
					<li class="">RT (Radiografico)</li>
					<li class="">MT (Magnetoscopico)</li>
				</ul>

				<p>Ispettori di saldatura qualificati operano nel rispetto degli standards nazionali ed internazionali(qualificati IIW).</p>

				<p>Il personale tecnico include anche ispettori qualificati ARAMCO,
					sia per le attività di ispezione presso il fornitore, sia per le attività in cantiere. </p>

				<p>Tutto il personale TECHNO SERVICE opera con le coperture assicurative di legge ed in ottemperanza
					alle disposizioni di legge previste in materia di sicurezza (legge 81/2008) ed alle disposizioni
					di legge locali previste nel Paese in cui il ns. personale andr&agrave; ad operare.
				</p>
			</div>
		</div>
	</div>
</section>
<!-- /.row -->



<section id="services-quality-section" class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="content" class="content"> <!-- // START content -->

					<h2>Techno Service - La Politica per la Qualità</h2>
					<p>TECHNO SERVICE intraprende da anni un percorso rivolto alla razionalizzazione
						ed al miglioramento dei processi interni ed alla soddisfazione delle esigenze del Cliente.</p>
					<p>Il modello  organizzativo e gestionale della Società, conforme alla norma UNI EN ISO 9001,
						ha consentito di ottenere la
						<a href="../../doc/Certificato_355.pdf" target="_new"> certificazione UNI EN ISO 9001:2015</a>
						rilasciata dall' Istituto Italiano della Saldatura.</p>
					<p>Al fine di perseguire uno sviluppo costante,  TECHNO SERVICE attribuisce la massima importanza
						alla continua implementazione ed al miglioramento del proprio Sistema di Gestione della Qualità
						nell'ottica di soddisfare i seguenti principi qualitativi:</p>

					<ul>
						<li>organizzazione orientata al Cliente </li>
						<li>leadership </li>
						<li>coinvolgimento del personale </li>
						<li>miglioramento continuo </li>
						<li>iniziative basate su elementi attendibili e concreti </li>

					</ul>

					<p>La ns. Società ritiene altresì importante l' adeguamento, lo sviluppo e l' applicazione,
						da parte di tutto il personale, dei criteri del  Sistema Gestione Qualità
						riferiti alla norma UNI EN ISO 9001:2015
						ed il raggiungimento degli obiettivi garantendo la totale soddisfazione del Cliente.</p>
					<p>A tale scopo viene richiesto e monitorato un costante sostegno ed un continuo coinvolgimento di
						tutta la struttura TECHNO SERVICE.</p>


				</div>
			</div>
		</div>
	</div>
</section>

<!--

<section id="contatti-about-section">
	<div class="container ">
		<div class="row">

			<div class="col-lg-6">
				<h2>Contatti e dati Aziendali</h2>

				<p>Techno Service Srl</p>
				<p>C.C.I.A.A. : 284212 - P. IVA : 02593920107</p>
				<p><i class="fas fa-map-marker-alt"></i> Torri Piane Centro Direzionale di San Benigno</p>
				<p style="margin-left:1.2rem;">Via Pietro Chiesa 9 - 16149, Genova - Italia</p>
				<p><i class="fas fa-phone"></i> +39.010.4691301</p>
				<p><i class="fas fa-fax"></i> +39.010.4691366</p>
				<p><i class="fas fa-envelope"></i> info@technoservicege.com</p>


			</div>
			<div class="col-lg-6">
				<section id="map-section" class="">
					<div class="container-fluid">
						<div id='map-home'></div>
					</div>
				</section>
			</div>

		</div>
		<div class="row">

			<div class="col-sm-12 text-center" style="display: flex;justify-content: center;margin-top: 1rem">
				<a href="skype:technoservicege?call">
					<div><i class="fab fa-skype fa-2x"></i> </div>
					<div>Contattaci su Skype</div>
				</a>
			</div>
		</div>

	</div>
</section>
-->
<!-- /.container -->
