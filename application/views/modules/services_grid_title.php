<div id="services-item-container">

	<?php
	$tot_services = count($servizi);
	for ($x = 0; $x < $tot_services; $x++): ?>

		<div class="service-item <?php echo $servizi[$x]["class"] ?>">
			<!--<a href="<?php /*echo $servizi[$x]["link"]*/ ?>"><img width="300" src="<?php /*echo $servizi[$x]["image"]*/ ?>"></a>-->
			<img width="300" src="<?php echo $servizi[$x]["image"] ?>">
			<div class="service-description">
				<div><i class="<?php echo $servizi[$x]["icon"] ?>"></i></div>
				<div><?php echo $servizi[$x]["nome"] ?></div>
			</div>
		</div>

	<?php endfor; ?>



</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<p class="text-center">
				<a class="btn btn-primary btn-lg" href="<?php echo $this->lang->line('route_services'); ?>" role="button"><?php echo $this->lang->line('string_service_button_scopri'); ?></a>
			</p>
		</div>
	</div>
</div>

