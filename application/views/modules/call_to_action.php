<?php
//se x non e' settato e' il primo accesso alla pagina allora la imposto a 0
//altrimenti la uso come indice per l'array

if (!isset($x)) {
	$x = 0;
}
if(isset($call_to_actions[$x])) {
	?>

	<section id="cta-container-section" class="calltoaction calltoaction-danger">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

					<div class="row">
						<div class="col-lg-9 cta-contents">
							<?php if (isset($call_to_actions[$x]['cta_title'])): ?>
								<h1 class="cta-title"><?php echo $call_to_actions[$x]['cta_title']; ?></h1>
							<?php endif; ?>
							<div class="cta-desc">
								<?php if (isset($call_to_actions[$x]['cta_text'])) {
									echo $call_to_actions[$x]['cta_text'];
								} ?>
							</div>
						</div>
						<div class="col-lg-3 cta-button">
							<a href="<?php if (isset($call_to_actions[$x]['cta_button_url'])) {
								echo $call_to_actions[$x]['cta_button_url'];
							} ?>"
							   class="btn btn-lg btn-block btn-light text-rosso"
							   title="<?php if (isset($call_to_actions[$x]['cta_button_url_title'])) {
								   echo $call_to_actions[$x]['cta_button_url_title'];
							   } ?>">
								<?php if (isset($call_to_actions[$x]['cta_button_text'])) {
									echo $call_to_actions[$x]['cta_button_text'];
								} else {
									echo "Clicca";
								} ?>
							</a>
						</div>
					</div>


				</div>
			</div>
		</div>
	</section>

	<?php
	$x++;
}


?>
