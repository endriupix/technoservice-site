<div class="row mt-4">
	<?php
	$tot_services = count($servizi);
	for ($x = 0; $x < $tot_services; $x++): ?>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="service-block">
			<div class="inner-box">
				<div class="image">
<!--					<a href="--><?php //echo $servizi[$x]["linkgallery"] ?><!--" class="btn-gallery"><img src="--><?php //echo $servizi[$x]["image"] ?><!--" alt="" /></a>-->
<!--					<div class="read-more"><a href="--><?php //echo $servizi[$x]["linkgallery"] ?><!--" class="btn-gallery">-->
<!--							--><?php //echo $this->lang->line('string_viewphoto'); ?>
<!--							<i class="--><?php //echo $servizi[$x]["icon"] ?><!--"></i></a>-->
<!--					</div>-->
				<img src="<?php echo $servizi[$x]["image"] ?>" alt="" />
				</div>
				<div class="lower-content">
<!--					<h3><a href="--><?php //echo $servizi[$x]["linkgallery"] ?><!--" class="btn-gallery">--><?php //echo $servizi[$x]["nome"] ?><!--</a></h3>-->
					<h3><?php echo $servizi[$x]["nome"] ?></h3>
					<div class="text">
						<?php echo $servizi[$x]["descrizione"]; ?>
						<ul class="mt-4">
						<?php for ($child = 0; $child < count($servizi[$x]["children"])-1; $child++): ?>
							<li><?php echo $servizi[$x]["children"][$child]; ?></li>
						<?php endfor; ?>
						</ul>
<!--						<a class="btn btn-primary float-left mb-5 btn-gallery" href="--><?php //echo $servizi[$x]["linkgallery"] ?><!--" role="button">-->
<!--							--><?php //echo $this->lang->line('string_gallery'); ?>
<!--						</a>-->
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php endfor; ?>



</div>


<?php /*******GALLERY CHE SI APRONO AL CLICK**************/ ?>


<div id="gallery-1" class="hidden">
	<a href="https://images.unsplash.com/photo-1462774603919-1d8087e62cad?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=0ebd884b4d6ac379f42146a2b26fbf2e">Image 1</a>
	<a href="https://images.unsplash.com/photo-1460499593944-39e14f96a8c6?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=d8bc3d45d5eeaaf4f576665707f4fddb">Image 2</a>
	<a href="https://images.unsplash.com/photo-1434434319959-1f886517e1fe?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=218dfdd2c0735dbd6ca0f718064a748b">Image 3</a>
	<a href="https://images.unsplash.com/photo-1431576901776-e539bd916ba2?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=a0941b28175909ca62f096eb533b0c97">Image 4</a>
</div>

<div id="gallery-2" class="hidden">
	<a href="https://images.unsplash.com/photo-1445295029071-5151176738d0?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=d1b06f7f69fb043b7d35a48180c5668d">Image 1</a>
	<a href="https://images.unsplash.com/photo-1439723680580-bfd9d28ef9b6?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=88b2a0312ad7711685c79139ae8f7cbe">Image 2</a>
	<a href="https://images.unsplash.com/photo-1434472007488-8d47f604f644?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=416049abdd7f2284b0651f00591ce215">Image 3</a>
	<a href="https://images.unsplash.com/photo-1450849608880-6f787542c88a?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=786a67dca1d8791d181bfd90b16240d9">Image 4</a>
</div>

<div id="gallery-3" class="hidden">
	<a href="https://images.unsplash.com/photo-1446776858070-70c3d5ed6758?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=9bbb8a4b7e9b0107fdb0fe52d0dbcaff">Image 1</a>
	<a href="https://images.unsplash.com/photo-1436407886995-41f8f5ee43ad?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=e7d757a5f131aa2fcaa01e41757b9165">Image 2</a>
	<a href="https://images.unsplash.com/photo-1442405740009-7b57cca9d316?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=8ad0e3579b66e754924530ded828521b">Image 3</a>
	<a href="https://images.unsplash.com/photo-1430651717504-ebb9e3e6795e?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=2f05d41e59b349313fdf09c041a768af">Image 4</a>
</div>

<div id="gallery-4" class="hidden">
	<a href="https://images.unsplash.com/photo-1446426156356-92b664d86b77?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=92e96949ac731e7cbcac943aa5934554">Image 1</a>
	<a href="https://images.unsplash.com/photo-1442037025225-e1cffaa2dc23?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=7fe04b68b0cb123bf568c6951c14b177">Image 2</a>
	<a href="https://images.unsplash.com/photo-1439565983992-3ee6dd957b9c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=a40b5d755a0cef9c019eb22ad635c72f">Image 3</a>
	<a href="https://images.unsplash.com/photo-1445964047600-cdbdb873673d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=c6665e8c83154faddeb6b9d253871486">Image 4</a>
</div>


<div id="gallery-5" class="hidden">
	<a href="https://images.unsplash.com/photo-1446426156356-92b664d86b77?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=92e96949ac731e7cbcac943aa5934554">Image 1</a>
	<a href="https://images.unsplash.com/photo-1442037025225-e1cffaa2dc23?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=7fe04b68b0cb123bf568c6951c14b177">Image 2</a>
	<a href="https://images.unsplash.com/photo-1439565983992-3ee6dd957b9c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=a40b5d755a0cef9c019eb22ad635c72f">Image 3</a>
	<a href="https://images.unsplash.com/photo-1445964047600-cdbdb873673d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=c6665e8c83154faddeb6b9d253871486">Image 4</a>
</div>


<div id="gallery-6" class="hidden">
	<a href="https://images.unsplash.com/photo-1446426156356-92b664d86b77?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=92e96949ac731e7cbcac943aa5934554">Image 1</a>
	<a href="https://images.unsplash.com/photo-1442037025225-e1cffaa2dc23?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=7fe04b68b0cb123bf568c6951c14b177">Image 2</a>
	<a href="https://images.unsplash.com/photo-1439565983992-3ee6dd957b9c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=a40b5d755a0cef9c019eb22ad635c72f">Image 3</a>
	<a href="https://images.unsplash.com/photo-1445964047600-cdbdb873673d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=c6665e8c83154faddeb6b9d253871486">Image 4</a>
</div>


<div id="gallery-7" class="hidden">
	<a href="https://images.unsplash.com/photo-1446426156356-92b664d86b77?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=92e96949ac731e7cbcac943aa5934554">Image 1</a>
	<a href="https://images.unsplash.com/photo-1442037025225-e1cffaa2dc23?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=7fe04b68b0cb123bf568c6951c14b177">Image 2</a>
	<a href="https://images.unsplash.com/photo-1439565983992-3ee6dd957b9c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=a40b5d755a0cef9c019eb22ad635c72f">Image 3</a>
	<a href="https://images.unsplash.com/photo-1445964047600-cdbdb873673d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=c6665e8c83154faddeb6b9d253871486">Image 4</a>
</div>



