<!DOCTYPE html>
<html lang="<?php echo $data_language["lang_abbr"]; ?>">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo $data_language["title"]; ?></title>

	<?php if(strcmp($page, "home") == 0 || strcmp($page, "contacts") == 0): ?>
	<script src='//api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js'></script>
	<link href='//api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css' rel='stylesheet' />
	<?php endif; ?>


	<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:400,600" rel="stylesheet">
	<?php if(strcmp($page, "services") == 0){ ?>
		<link href="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">
	<?php } ?>
	<link href="<?php echo site_url(); ?>/assets/css/app.min.css" rel="stylesheet">
	<link href="<?php echo site_url(); ?>/assets/css/<?php echo $page; ?>.min.css" rel="stylesheet">



<!--	--><?php //if(strcmp($page, "home") == 0) { ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/home.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "about") == 0){ ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/about.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "clients") == 0){ ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/clients.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "contacts") == 0){ ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/contacts.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "privacy") == 0){ ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/privacy.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "services") == 0){ ?>
<!--		<link href="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">-->
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/services.min.css" rel="stylesheet">-->
<!--	--><?php //}elseif(strcmp($page, "works") == 0){ ?>
<!--		<link href="--><?php //echo site_url(); ?><!--/assets/css/works.min.css" rel="stylesheet">-->
<!--	--><?php //} ?>



	<link rel="canonical" href="<?php echo $data_language["canonical"]; ?>" />
	<link rel="alternate" hreflang="en" href="<?php echo $data_language["hreflang_en"]; ?>" />
	<link rel="alternate" hreflang="it" href="<?php echo $data_language["hreflang_it"]; ?>" />
	<link rel="alternate" hreflang="x-default" href="<?php echo $data_language["x_default"]; ?>" />

</head>
<body class="<?php echo $page; ?>">

