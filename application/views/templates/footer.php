</main>
<!-- Footer -->
<footer class="py-5 text-bianco">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-lg-3 text-left">
				<h3><?php echo $this->lang->line('page_footer_contacts_intro_title'); ?></h3>
				<hr class="hr-default-white">
				<div>
					<a href="/"><img src="<?php echo DIR_IMG; ?>logo.png" class='logo' alt="<?php echo $this->lang->line('image_logo_alt'); ?>"></a>
					<?php echo $this->lang->line('page_footer_contacts_intro'); ?>
					<p><a href="/<?php echo $this->lang->line('route_contacts'); ?>"><?php echo $this->lang->line('string_contact_us'); ?></a></p>
				</div>
			</div>
			<div class="col-md-8 col-lg-6 text-left">
				<div class="row">
					<div class="col-md-6">
						<?php echo $this->lang->line('page_footer_contacts_main_office_title'); ?>
						<hr class="hr-default-white">
						<?php echo $this->lang->line('page_footer_contacts_main_office'); ?>
					</div>
					<div class="col-md-6">
						<?php echo $this->lang->line('page_footer_contacts_registered_office_title'); ?>
						<hr class="hr-default-white">
						<?php echo $this->lang->line('page_footer_contacts_registered_office'); ?>

					</div>

				</div>

			</div>
			<div class="col-lg-3 text-left" id="menu-useful-link">
				<h3><b><?php echo $this->lang->line('page_footer_menu_header'); ?></b></h3>
				<hr class="hr-default-white">
				<!--				<p class="titlefooter"><b>--><?php //echo $this->lang->line('string_links_utili'); ?><!--</b></p>-->
				<p><a href="/<?php echo $this->lang->line('route_privacy'); ?>"><?php echo $this->lang->line('seo_privacy_menu_link_title'); ?></a></p>
				<p><a href="/<?php echo $this->lang->line('route_cookiepolicy'); ?>"><?php echo $this->lang->line('seo_cookiepolicy_menu_link_title'); ?></a></p>
				<p><a href="/<?php echo $this->lang->line('route_work'); ?>"><?php echo $this->lang->line('seo_work_menu_link_title'); ?></a></p>
				<p><a href="/documents/Certificato_355_UNI_EN_ISO_9001_2015.pdf"><?php echo $this->lang->line('seo_certificato_iso_title'); ?></a></p>

				<p><a href="" title="<?php echo $this->lang->line('page_footer_other_office_title'); ?>"><?php echo $this->lang->line('page_footer_other_office'); ?></a></p>
			</div>

		</div>

		<div class="text-center mt-4 mb-4">
			<?php echo $this->lang->line('page_footer_contacts_works_tag'); ?>
		</div>
		<p class="mt-3 text-center text-white ">

			Copyright 2019 &copy; TechnoService s.r.l. All rights reserved.
			<br><a href="https://creativestorming.com">Realizzazione siti Web e design</a>
			by <a href="https://creativestorming.com"> Creative Storming
				<img src="https://creativestorming.com/img/brand/Logo-CreativeStorming-Negativo.png" style="width: 40px; vertical-align: middle;">
			</a>
		</p>



	</div>
	<!-- /.container -->
</footer>


<!-- Bootstrap core JavaScript -->
<script src="/vendor/jquery/jquery.min.js"></script>
<!--da caricare solo in homepage-->


<!--da caricare solo in homepage-->

<!--da caricare solo in about-->
<?php if(strcmp($page, "about") == 0) : ?>
	<script src="https://cdn.jsdelivr.net/npm/scrollreveal@4.0.5/dist/scrollreveal.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<?php endif; ?>

<!--da caricare solo in about-->


<!--da caricare solo in serrvizi-->
<?php if(strcmp($page, "services") == 0) : ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<?php endif; ?>

<!--da caricare solo in serrvizi-->

<script src="/vendor/bootstrap/bootstrap.min.js"></script>


<?php if(strcmp($page, "home") == 0 || strcmp($page, "clients") == 0) : ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<?php endif; ?>


<script src="/assets/js/app.js"></script>
<script src='/assets/js/<?php echo $page; ?>.js'></script>
<!--<script src="/js/vendor.js"></script>-->																																															->

</body>

</html>
