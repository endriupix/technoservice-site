<div id="topbar-info-section" class="bkg-nero text-bianco">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-left">
				<i class="fas fa-phone-square text-grigio-medium"></i> <span
					class="text-grigio-medium"><?php echo $this->lang->line('string_contact_us'); ?>
					:</span> <?php echo TELEFONO; ?>
			</div>
			<div class="col-md-7 col-right">
				<i class="fas fa-map-marker-alt text-grigio-medium"></i> <span
					class="text-grigio-medium">Main Office:</span> <?php echo INDIRIZZO; ?>&nbsp;|&nbsp;

				<?php if (strcmp($data_language["lang_abbr"], "en") == 0): ?>

					<a class="" href="<?php echo $data_language["hreflang_it"]; ?>">
						<img width="23" src="<?php echo DIR_IMG . $data_language["lang_flag_it"]; ?>"
							 alt="<?php echo $this->lang->line('image_flag_it_alt'); ?>">
					</a>
				<?php else: ?>
					<a class="" href="<?php echo $data_language["hreflang_en"]; ?>">
						<img width="23" src="<?php echo DIR_IMG . $data_language["lang_flag_en"]; ?>"
							 alt="<?php echo $this->lang->line('image_flag_en_alt'); ?>">
					</a>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
<div id="topbar-logo-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<a href="/"><img class="logo" src="<?php echo DIR_IMG; ?>logo.png"
								 alt="<?php echo $this->lang->line('image_logo_alt'); ?>"></a>
			</div>
			<div class="col-md-8">
				<div class="topbar-certification text-left" style="">
					<div><img src="<?php echo DIR_IMG ?>certification-techno-service.png" width="50" height="50">
						<div><span class="text-small">Azienda Qualificata<br>ARAMCO</span></div>
					</div>
					<div><img src="<?php echo DIR_IMG ?>certification-techno-service.png" width="50" height="50" alt="">
						<div><span class="text-small">Azienda Certificata<br>ISO 9001 - 2015</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Navigation -->
<nav id="menu-sito" class="navbar navbar-expand-lg navbar-dark bkg-rosso">
	<div class="container">
		<!--        <a class="navbar-brand" href="index.html">TechnoService SRL</a>-->
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
				aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="/<?php if (strcmp($data_language["lang_abbr"], 'en') == 0) {
						echo 'en';
					} ?>">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="/<?php echo $this->lang->line('route_about'); ?>"><?php echo $this->lang->line('seo_about_menu_link_title'); ?></a>
				</li>
				<!--				<li class="nav-item dropdown">-->
				<!--					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"-->
				<!--					   aria-haspopup="true" aria-expanded="false">-->
				<!--						Azienda-->
				<!--					</a>-->
				<!--					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">-->
				<!--						<a class="dropdown-item" href="/-->
				<?php //echo $this->lang->line('route_about'); ?><!--">-->
				<?php //echo $this->lang->line('seo_about_menu_link_title'); ?><!--</a>-->
				<!--						<a class="dropdown-item"  href="/-->
				<?php //echo $this->lang->line('route_work'); ?><!--">-->
				<?php //echo $this->lang->line('seo_work_menu_link_title'); ?><!--</a>-->
				<!--					</div>-->
				<!--				</li>-->
				<li class="nav-item">
					<a class="nav-link"
					   href="/<?php echo $this->lang->line('route_services'); ?>"><?php echo $this->lang->line('seo_services_menu_link_title'); ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="/<?php echo $this->lang->line('route_clients'); ?>"><?php echo $this->lang->line('seo_clients_menu_link_title'); ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="/<?php echo $this->lang->line('route_contacts'); ?>"><?php echo $this->lang->line('seo_contacts_menu_link_title'); ?></a>
				</li>


			</ul>
		</div>
	</div>
</nav>

