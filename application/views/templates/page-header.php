<header>
	<div class="jumbotron text-center internal-page">
        <h1 class="display-4 mt-4 mb-3">
			<?php echo $title ?>
            <?php if(isset($subtitle)): ?><small>Subheading</small><?php endif; ?>
        </h1>

    </div>

</header>
<main>
