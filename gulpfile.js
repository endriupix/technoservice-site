'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var pkg = require('./package.json');
var rename = require("gulp-rename");
var sass = require('gulp-sass');
sass.compiler = require('node-sass');

// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function() {

  // Bootstrap
  // gulp.src([
  //     './node_modules/bootstrap/dist/**/*',
  //     './node_modules/bootstrap/dist/css/bootstrap-grid*',
  //     './node_modules/bootstrap/dist/css/bootstrap-reboot*'
  //   ])
  //   .pipe(gulp.dest('./vendor/bootstrap'));
    gulp.src([
      './node_modules/bootstrap/dist/js/*'
    ])
    .pipe(gulp.dest('./vendor/bootstrap'));

  // jQuery
  gulp.src([
      './node_modules/jquery/dist/*',
      './node_modules/jquery/dist/core.js'
    ])
    .pipe(gulp.dest('./vendor/jquery'));

    // Normalize
  gulp.src([
      './node_modules/normalize.css/normalize.css'
    ])
    .pipe(gulp.dest('./vendor/normalize'));

});

// Default task
gulp.task('default', ['vendor']);

// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

// Dev task
gulp.task('dev', ['browserSync'], function() {
  gulp.watch('./assets/css/*.css', browserSync.reload);
  gulp.watch('./*.html', browserSync.reload);
  gulp.watch('./*.php', browserSync.reload);

});


gulp.task('sass', function() {
    gulp.src('./assets/scss/app.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/about.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/clients.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/contacts.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/home.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/privacy.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/cookiepolicy.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/services.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));
	gulp.src('./assets/scss/work.scss', {style: 'compressed'})
		.pipe(rename({suffix: '.min'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'));

});

gulp.task('sass:watch', function () {
    gulp.watch('./assets/scss/**/*.scss', ['sass']);
});
